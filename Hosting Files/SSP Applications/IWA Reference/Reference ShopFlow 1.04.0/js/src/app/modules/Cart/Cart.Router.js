// Cart.Router.js
// --------------
// Creates the cart route
define('Cart.Router', ['Cart.Views'], function (Views)
{
	'use strict';
	
	return Backbone.Router.extend({
		
		routes: {
			'cart': 'showCart'
		,	'cart?*options': 'showCart'
		}
		
	,	initialize: function (Application)
		{
			this.application = Application;
		}
		
	,	showCart: function ()
		{
			this.renderView(Views);
		}

	,	renderView: function (CartView)
		{
			var self = this;
			self.application.loadCart()
				.done(function ()
				{
					var view = new CartView.Detailed({
						model: self.application.getCart()
					,	application: self.application
					});
					
					view.showContent();
				});
		}
	});
});

