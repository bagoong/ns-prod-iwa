<% var invoices = view.invoices; %>
<div>
<% if (invoices.length) { %>
	<div class="payment-table">
		<% invoices.each(function (invoice) { %>
			<%= paymentWizardInvoice(invoice) %>
		<% }); %>
	</div>
<% } else { %>
	<p class="invoice-list-empty-list"><%= _('You don\'t have any Open Invoices at the moment,</br>see <a href="/paid-invoices" class="InvoicePaidInFull">Invoices Paid In Full</a>').translate() %></p>
<% } %>
</div>