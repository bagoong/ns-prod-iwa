/*jshint laxcomma:true*/
define(
	['LivePayment.Model', 'Invoice.Model','Deposit.Model','CreditMemo.Model','Invoice.Collection', 'Application', 'Utils']
,	function (LivePaymentModel, InvoiceModel, DepositModel, CreditMemoModel)
{
	'use strict';

	return describe('PaymentModel Module', function ()
	{
		describe('Distribution algorithm', function ()
		{
			function testAdd (options)
			{
				var invoiceData = options.invoiceData
				,	depositData = options.depositData
				,	creditmemoData = options.creditmemoData
				,	expected_deposit_total = options.expected_deposit_total
				,	expected_creditmemo_total = options.expected_creditmemo_total
				,	expected_apply_deposits = options.expected_apply_deposits
				,	expected_apply_creditmemos = options.expected_apply_creditmemos

				,	application = SC.Application('MyAccount')
				,	pm = new LivePaymentModel({}, {application: application})
				,	invoices = pm.get('invoices')
				,	deposits = pm.get('deposits')
				,	creditmemos = pm.get('creditmemos')
				,	id = 1
				,	total_invoice = 0
				,	now = new Date().getTime();

				pm.distributePaymentsFIFO();

				expect(invoices.length).toEqual(0);
				expect(deposits.length).toEqual(0);
				expect(creditmemos.length).toEqual(0);

				expect(pm.get('payment')).toEqual(0);
				expect(pm.get('invoices_total')).toEqual(0);
				expect(pm.get('deposits_total')).toEqual(0);
				expect(pm.get('creditmemos_total')).toEqual(0);

				_.each(invoiceData, function (inv)
				{
					total_invoice += inv.payment;

					invoices.add(
						new InvoiceModel({
							due: inv.payment
						,	id: id
						,	internalid: id
						,	tranid: id + 1
						,	duedate: (inv.duedate instanceof Date ? inv.duedate : new Date(now + inv.duedate))
						})
					);

					pm.selectInvoice(id);

					expect(pm.get('payment')).toEqual(total_invoice);
					expect(pm.get('invoices_total')).toEqual(total_invoice);
					expect(pm.get('deposits_total')).toEqual(0);
					expect(pm.get('creditmemos_total')).toEqual(0);

					id++;
				});

				id = 1;
				_.each(depositData, function (dep)
				{
					deposits.add(
						new DepositModel({
							amountremaining: dep.amountremaining
						,	internalid: id
						,	tranid: id++
						,	datecreated: (dep.datecreated instanceof Date ? dep.datecreated : new Date(now + dep.datecreated))
						})
					);

					expect(pm.get('invoices_total')).toEqual(total_invoice);
					expect(pm.get('creditmemos_total')).toEqual(0);
				});

				id = 1;
				_.each(creditmemoData, function (cremem)
				{
					creditmemos.add(
						new CreditMemoModel({
							amountremaining: cremem.amountremaining
						,	internalid: id
						,	tranid: id++
						,	trandate: (cremem.trandate instanceof Date ? cremem.trandate : new Date(now + cremem.trandate))
						})
					);

					expect(pm.get('invoices_total')).toEqual(total_invoice);
				});

				function checkAll()
				{
					expect(invoices.length).toEqual(invoiceData.length);
					expect(deposits.length).toEqual(depositData.length);
					expect(creditmemos.length).toEqual(creditmemoData.length);

					expect(pm.get('payment')).toEqual(total_invoice - expected_deposit_total - expected_creditmemo_total);
					expect(pm.get('invoices_total')).toEqual(total_invoice);
					expect(pm.get('deposits_total')).toEqual(expected_deposit_total);
					expect(pm.get('creditmemos_total')).toEqual(expected_creditmemo_total);

					var i = 0
					,	j = 0
					,	expected_apply = {}
					,	apply_row = {};

					for (i = 0; i < expected_apply_deposits.length; i++)
					{
						var deposit = deposits.where({internalid: i + 1})[0]
						,	deposit_expected_apply = expected_apply_deposits[i];

						expect(deposit.get('apply').length).toEqual(deposit_expected_apply.length);

						for (j = 0; j < deposit_expected_apply.length; j++)
						{
							expected_apply = deposit_expected_apply[j];
							apply_row = deposit.get('apply')[j];

							expect(apply_row.amount).toEqual(expected_apply.amount);
							expect(apply_row.invoice).toEqual(expected_apply.invoice);
							expect(apply_row.refnum).toEqual(expected_apply.refnum);
						}
					}

					for (i = 0; i < expected_apply_creditmemos.length; i++)
					{
						var creditmemo = creditmemos.where({internalid: i+1})[0]
						,	creditmemo_expected_apply = expected_apply_creditmemos[i];

						expect(creditmemo.get('apply').length).toEqual(creditmemo_expected_apply.length);

						for (j = 0; j < creditmemo_expected_apply.length; j++)
						{
							expected_apply = creditmemo_expected_apply[j];
							apply_row = creditmemo.get('apply')[j];

							expect(apply_row.amount).toEqual(expected_apply.amount);
							expect(apply_row.invoice).toEqual(expected_apply.invoice);
							expect(apply_row.refnum).toEqual(expected_apply.refnum);
						}
					}
				}

				pm.distributePaymentsFIFO();
				checkAll();

				return pm;
			}

			it('Test initialize', function ()
			{
				testAdd({
					invoiceData: []
				,	depositData: []
				,	creditmemoData: []
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding invoices only', function ()
			{
				var now = new Date().getTime();
				testAdd({
					invoiceData: [
						{payment: 20, duedate: 10}
					,	{payment: 10, duedate: -10}
					]
				,	depositData: []
				,	creditmemoData: []
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding deposits only', function ()
			{
				testAdd({
					invoiceData: []
				,	depositData: [
						{amountremaining: 20, datecreated: 10}
					,	{amountremaining: 10, datecreated: -10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding credit memos only', function ()
			{
				testAdd({
					invoiceData: []
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 20, trandate: 20}
					,	{amountremaining: 10, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 1 invoice < 1 deposit', function ()
			{
				testAdd({
					invoiceData: [{payment: 20, duedate: 10}]
				,	depositData: [{amountremaining: 30, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 20
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 20, invoice: 1, refnum: 2}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 1 invoice > 1 deposit', function ()
			{
				testAdd({
					invoiceData: [{payment: 30, duedate: 10}]
				,	depositData: [{amountremaining: 20, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 20
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 20, invoice: 1, refnum: 2}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [{amountremaining: 20, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 20
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 20, invoice: 1, refnum: 2}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit inverse dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [{amountremaining: 20, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 20
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 20, invoice: 2, refnum: 3}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit cover 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [{amountremaining: 30, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 30
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 30, invoice: 1, refnum: 2}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit inverse dates cover 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [{amountremaining: 30, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 30
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[{amount: 30, invoice: 2, refnum: 3}]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [{amountremaining: 40, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 40
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[
						{amount: 30, invoice: 1, refnum: 2}
					,	{amount: 10, invoice: 2, refnum: 3}
					]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit inverse dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [{amountremaining: 40, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 40
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[
						{amount: 30, invoice: 2, refnum: 3}
					,	{amount: 10, invoice: 1, refnum: 2}
					]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [{amountremaining: 100, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[
						{amount: 30, invoice: 1, refnum: 2}
					,	{amount: 30, invoice: 2, refnum: 3}
					]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 1 deposit inverse dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [{amountremaining: 100, datecreated: 10}]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[
						{amount: 30, invoice: 2, refnum: 3}
					,	{amount: 30, invoice: 1, refnum: 2}
					]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 10, datecreated: 10}
					,	{amountremaining: 5, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 15
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[{amount: 10, invoice: 1, refnum: 2}]
					,	[{amount: 5, invoice: 1, refnum: 2}]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [
						{amountremaining: 10, datecreated: 10}
					,	{amountremaining: 5, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 15
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[{amount: 10, invoice: 2, refnum: 3}]
					,	[{amount: 5, invoice: 2, refnum: 3}]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse deposit dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 10, datecreated: 10}
					,	{amountremaining: 5, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 15
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[{amount: 10, invoice: 1, refnum: 2}]
					,	[{amount: 5, invoice: 1, refnum: 2}]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice and deposit dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 10, datecreated: 20}
					,	{payment: 5, datecreated: 10}
					]
				,	depositData: [
						{amountremaining: 30, duedate: 20}
					,	{amountremaining: 30, duedate: 10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 15
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [[
						{amount: 10, invoice: 1, refnum: 2}
					,	{amount: 5, invoice: 2, refnum: 3}
					]]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 10}
					,	{amountremaining: 5, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 45
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 5, invoice: 2, refnum: 3}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 10}
					,	{amountremaining: 5, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 45
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 5, invoice: 1, refnum: 2}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse deposit dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 20}
					,	{amountremaining: 5, datecreated: 10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 45
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 5, invoice: 2, refnum: 3}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice and deposit dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 20}
					,	{amountremaining: 5, datecreated: 10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 45
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 5, invoice: 1, refnum: 2}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 10}
					,	{amountremaining: 40, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 20, invoice: 2, refnum: 3}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 10}
					,	{amountremaining: 40, datecreated: 20}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 20, invoice: 1, refnum: 2}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse deposit dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 20}
					,	{amountremaining: 40, datecreated: 10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 20, invoice: 2, refnum: 3}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 2 invoice and 2 deposit inverse invoice and deposit dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: [
						{amountremaining: 40, datecreated: 20}
					,	{amountremaining: 40, datecreated: 10}
					]
				,	creditmemoData: []
				,	expected_deposit_total: 60
				,	expected_creditmemo_total: 0
				,	expected_apply_deposits: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 20, invoice: 1, refnum: 2}
						]
					]
				,	expected_apply_creditmemos: []
				});
			});

			it('Test adding 1 invoice < 1 credit memo', function ()
			{
				testAdd({
					invoiceData: [{payment: 20, duedate: 10}]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 30, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 20
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 20, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 1 invoice > 1 credit memo', function ()
			{
				testAdd({
					invoiceData: [{payment: 30, duedate: 10}]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 20, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 20
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 20, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 20, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 20
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 20, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo inverse dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 20, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 20
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 20, invoice: 2, refnum: 3}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo cover 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 30, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 30
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo inverse dates cover 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 30, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 30
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 2, refnum: 3}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 40, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 40
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 1, refnum: 2}
					,	{amount: 10, invoice: 2, refnum: 3}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo inverse dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 40, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 40
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 2, refnum: 3}
					,	{amount: 10, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 100, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 1, refnum: 2}
					,	{amount: 30, invoice: 2, refnum: 3}
					]]
				});
			});

			it('Test adding 2 invoice and 1 credit memo inverse dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [{amountremaining: 100, trandate: 10}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [[
						{amount: 30, invoice: 2, refnum: 3}
					,	{amount: 30, invoice: 1, refnum: 2}
					]]
				});
			});

			it('Test adding 2 invoice and 2 credit memo cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 10, trandate: 10}
					,	{amountremaining: 5, trandate: 20}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 15
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[{amount: 10, invoice: 1, refnum: 2}]
					,	[{amount: 5, invoice: 1, refnum: 2}]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memo inverse invoice dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 10, trandate: 10}
					,	{amountremaining: 5, trandate: 20}]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 15
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[{amount: 10, invoice: 2, refnum: 3}]
					,	[{amount: 5, invoice: 2, refnum: 3}]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memo inverse dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 10, trandate: 20}
					,	{amountremaining: 5, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 15
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[{amount: 10, invoice: 1, refnum: 2}]
					,	[{amount: 5, invoice: 1, refnum: 2}]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memo inverse invoice and  credit memo dates cover less than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 10, trandate: 20}
					,	{amountremaining: 5, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 15
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[{amount: 10, invoice: 2, refnum: 3}]
					,	[{amount: 5, invoice: 2, refnum: 3}]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memo cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 10}
					,	{amountremaining: 5, trandate: 20}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 45
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 5, invoice: 2, refnum: 3}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse invoice dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData:[] 
				,	creditmemoData: [
						{amountremaining: 40, trandate: 10}
					,	{amountremaining: 5, trandate: 20}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 45
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 5, invoice: 1, refnum: 2}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 20}
					,	{amountremaining: 5, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 45
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 5, invoice: 2, refnum: 3}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse invoice and credit memo dates cover more than 1 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData:[] 
				,	creditmemoData: [
						{amountremaining: 40, trandate: 20}
					,	{amountremaining: 5, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 45
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 5, invoice: 1, refnum: 2}
						]
					]
				});
			});
			
			it('Test adding 2 invoice and 2 credit memos cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 10}
					,	{amountremaining: 40, trandate: 20}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 20, invoice: 2, refnum: 3}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse invoice dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 10}
					,	{amountremaining: 40, trandate: 20}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 20, invoice: 1, refnum: 2}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse credit memo dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 10}
					,	{payment: 30, duedate: 20}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 20}
					,	{amountremaining: 40, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 1, refnum: 2}
						,	{amount: 10, invoice: 2, refnum: 3}
						]
					,	[
							{amount: 20, invoice: 2, refnum: 3}
						]
					]
				});
			});

			it('Test adding 2 invoice and 2 credit memos inverse invoice and credit memo dates cover more than 2 invoice', function ()
			{
				testAdd({
					invoiceData: [
						{payment: 30, duedate: 20}
					,	{payment: 30, duedate: 10}
					]
				,	depositData: []
				,	creditmemoData: [
						{amountremaining: 40, trandate: 20}
					,	{amountremaining: 40, trandate: 10}
					]
				,	expected_deposit_total: 0
				,	expected_creditmemo_total: 60
				,	expected_apply_deposits: []
				,	expected_apply_creditmemos: [
						[
							{amount: 30, invoice: 2, refnum: 3}
						,	{amount: 10, invoice: 1, refnum: 2}
						]
					,	[
							{amount: 20, invoice: 1, refnum: 2}
						]
					]
				});
			});
		});
	});
});