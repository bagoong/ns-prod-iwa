define('PaymentWizard.Module.Summary', ['Wizard.Module'], function (WizardModule)
{
	'use strict';

	return WizardModule.extend({
 
		template: 'payment_wizard_summary_module'

	,	initialize: function (options)
		{
			this.isActive = false;
			this.options = options;
			this.wizard = options.wizard;

			this.wizard.model.on('changeApply', jQuery.proxy(this, 'render'));
		}

	,	future: function ()
		{
			this.isActive = false;
		}

	,	present: function ()
		{
			this.isActive = true;
		}

	,	past: function ()
		{
			this.isActive = false;
		}

	,	render: function ()
		{
			this.continueButtonDisabled = '';
			if (this.options.submit)
			{
				this.continueButtonLabel = _('Submit').translate();
			}
			else
			{
				var selected_invoices = this.wizard.model.getSelectedInvoices();
				if (selected_invoices.length)
				{
					this.continueButtonLabel = _('$(0) Selected: Next').translate(selected_invoices.length);
				}
				else
				{
					this.continueButtonDisabled = 'disabled="disabled"';
					this.continueButtonLabel = _('0 Selected').translate();
				}
			}

			this._render();

			if (this.isActive)
			{
				this.trigger('change_enable_continue', !this.continueButtonDisabled);
				this.trigger('change_label_continue', this.continueButtonLabel);
			}
		}

	,	getEstimatedPayment: function ()
		{
			return  _.formatCurrency(this.wizard.model.get('payment'));
		}
	});
});