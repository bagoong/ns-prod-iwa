// OrderWizard.EditDeposit.View.js
// --------------------
//
define('PaymentWizard.EditAmount.View', function ()
{
	'use strict';

	return Backbone.View.extend({

		template: 'payment_wizard_edit_amount_layout'

	,	events:
		{
			'submit [data-action="edit-amount-form"]':'modifyAmountToPay'
		,	'change [name="amount"]':'changedAmountToPay'
		}

	,	changedAmountToPay: function(e)
		{
			var value = parseFloat(jQuery(e.target).val());

			if (this.model.get('discountapplies'))
			{
				if (value === this.model.get('due'))
				{
					this.$('.discountSection').show();
					this.$('.discountWarning').hide();
				}
				else
				{
					this.$('.discountSection').hide();
					this.$('.discountWarning').show();
				}
			}
		}


	,	initialize: function (options)
		{
			this.parentView = options.parentView;
			this.model = options.model;

			if (options.type === 'invoice')
			{
				this.original_amount_attribute = 'total';
				this.amount_due_attribute = 'due';
				this.input_label = _('Amount to Pay').translate();
				this.original_amount_label  = _('Original Amount').translate();
				this.amount_due_label  = _('Amount Due').translate();
				this.page_header = _('Invoice #$(0)').translate(this.model.get('refnum'));
				this.title = _('Amount to pay for invoice #$(0)').translate(this.model.get('refnum'));
			}
			else if (options.type === 'deposit')
			{
				this.input_label = _('Amount to apply').translate();
				this.original_amount_label = _('Deposit remaining amount').translate();
				this.page_header = _('Deposit #$(0)').translate(this.model.get('refnum'));
				this.title = _('Amount to apply for deposit #$(0)').translate(this.model.get('refnum'));
			}
			else if (options.type === 'creditmemo')
			{
				this.input_label = _('Amount to apply').translate();
				this.original_amount_label  = _('Credit memo remaining amount').translate();
				this.page_header = _('Credit Memo #$(0)').translate(this.model.get('refnum'));
				this.title = _('Amount to apply for credit memo #$(0)').translate(this.model.get('refnum'));
			}

			this.page_header = '<b>'+this.page_header.toUpperCase()+'</b>';
		}

	,	modifyAmountToPay: function (e)
		{
			e.preventDefault();

			var self = this
			,	value = parseFloat(this.$('.amountToPayInput').val());

			if (this.model.get('discountapplies') && value === this.model.get('due'))
			{
				value = this.model.get('duewithdiscount');
			}
			
			this.model.set('amount', value, {
				validate: true
			});

			if (this.model.isValid())
			{
				self.model.set('amount_formatted',_.formatCurrency(value));
				self.parentView.wizard.model.distributePaymentsFIFO();
				self.$containerModal.modal('hide');
				self.destroy();
			}
		}
	});
});