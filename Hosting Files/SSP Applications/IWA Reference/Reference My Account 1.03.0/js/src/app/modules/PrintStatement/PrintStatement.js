// PrintStatement.js
// -----------------
// Defines the Print Statement module
define('PrintStatement', ['PrintStatement.Model','PrintStatement.Views','PrintStatement.Router'], function (Model,Views, Router)
{
	'use strict';

	return	{
		Views: Views
	,	Router : Router
	,	Model: Model

	,	mountToApp: function (application, options)
		{
			return new Router(application);
		}
	};
});
