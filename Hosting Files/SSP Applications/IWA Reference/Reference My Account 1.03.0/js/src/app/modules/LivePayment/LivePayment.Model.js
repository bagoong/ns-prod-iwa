define('LivePayment.Model', ['Invoice.Collection', 'Deposit', 'CreditMemo','Address.Collection', 'OrderPaymentmethod.Collection'], 
function (InvoiceCollection, Deposit, CreditMemo, AddressesCollection, OrderPaymentmethodCollection)
{
	'use strict';

	return Backbone.Model.extend({

		urlRoot: 'services/live-payment.ss'

	,	initialize: function (attributes, options)
		{
			this.application = options.application;

			var user = this.application.getUser();

			this.set('invoices_total', 0);
			this.on('sync', function (model) {
				model.set('invoices_total', 0);
			});

			this.on('change:addresses', function (model, addresses)
			{
				model.set('addresses', new AddressesCollection(addresses), {silent: true});
			});
			this.trigger('change:addresses', this, attributes && attributes.addresses || []);

			this.on('change:paymentmethods', function (model, paymentmethods)
			{
				model.set('paymentmethods', new OrderPaymentmethodCollection(paymentmethods), {silent: true});
			});
			this.trigger('change:paymentmethods', this, attributes && attributes.paymentmethod || []);

			this.on('change:invoices', function (model, invoices)
			{
				model.set('invoices', new InvoiceCollection(invoices), {silent: true});
				
				model.trigger('invoicesUpdated');
				model.trigger('changeApply');
			});
			this.trigger('change:invoices', this, attributes && attributes.invoices || []);

			this.on('change:deposits', function (model, deposits)
			{
				model.set('deposits', new InvoiceCollection(deposits), {silent: true});
			});
			this.trigger('change:deposits', this, attributes && attributes.deposits || []);

			this.on('change:creditmemos', function (model, creditmemos)
			{
				model.set('creditmemos', new InvoiceCollection(creditmemos), {silent: true});
			});
			this.trigger('change:creditmemos', this, attributes && attributes.creditmemos || []);

			this.on('change:balance', function(model)
			{
				user.set('balance', model.get('balance'));
				user.set('balance_formatted', model.get('balance_formatted'));
			});

		}

	,	getSelectedInvoices: function()
		{
			return new InvoiceCollection(this.get('invoices').filter(function (invoice)
			{
				return invoice.get('apply');
			}));
		}

	,	selectInvoice: function (invoice)
		{	
			invoice = this.get('invoices').get(invoice);

			if (invoice && invoice.get('due'))
			{
				// marks the invoice as checked
				invoice.set('apply', true);
				invoice.set('checked', true);
				var amount =  invoice.get('amount') ? invoice.get('amount') : invoice.get('due');
				invoice.set('amount', amount);
				invoice.set('amount_formatted', _.formatCurrency(amount));
			}

			return this.distributePaymentsFIFO();
		}
		
	,	unselectInvoice: function (invoice)
		{
			invoice = this.get('invoices').get(invoice);

			if (invoice)
			{
				invoice.set('apply', false);
				invoice.set('checked', false);
			}

			return this.distributePaymentsFIFO();
		}

	,	normalizeDate: function (date)
		{
			if (!date)
			{
				return;
			}
			else if (date instanceof Date)
			{
				return date.getTime();
			}
			else if (typeof date === 'string')
			{
				// TODO: Consider company's date format here
				return Date.parse(date);
			}
			else if (typeof date === 'number')
			{
				return date;
			}
		}

		/*
		Distributes deposits then credit memos in order by date (first in frist out)
		This way the maximum amount of oldest deposit allowed will cover the oldest
		invoice. Then the next deposit and so on. If no more deposits are available
		for that invoice, credit memos will be considered in the same fashion and so on.
		If memos and deposits do not cover the total amount to be paid of all
		invoices, then it will put that amount in the remaining attribute.
		Assumes that amounts are in the same currency and that they do not exceed
		the amounts stored in the records (they are possible) and that rounding issues
		will not arise from substraction and addition operations and that entities
		appear only once (unique ids on each)
		*/
	,	distributePaymentsFIFO: function ()
		{
			// First thing is to order everything by date and initialize parameters
			var	self = this
			,	invoices = new InvoiceCollection(this.getSelectedInvoices().sortBy(function (invoice)
				{
					return self.normalizeDate(invoice.get('duedate'));
				}))
			,	deposits = this.get('deposits')
			,	credit_memos = this.get('creditmemos')
			,	invoice_amount = 'amount'
			,	deposit_amount = 'amountremaining'
			,	credit_memo_amount = 'amountremaining'
			,	invoices_total = 0
			,	deposits_total = 0
			,	creditmemos_total = 0;

			invoices.each(function (invoice)
			{
				invoice.set('remaining', invoice.get(invoice_amount));
				invoices_total += invoice.get(invoice_amount);
			});

			deposits.each(function (deposit)
			{
				deposit.set('apply', []);
				deposit.set('remaining', deposit.get(deposit_amount));
			});

			credit_memos.each(function (credit_memo)
			{
				credit_memo.set('apply', []);
				credit_memo.set('remaining', credit_memo.get(credit_memo_amount));
			});

			//Then apply remaining deposits to complete
			deposits.each(function (deposit)
			{
				if (deposit.get('remaining') > 0)
				{
					invoices.each(function (invoice)
					{
						var amount = Math.min(invoice.get('remaining'), deposit.get('remaining'));

						if (amount > 0)
						{
							deposit.get('apply').push({
								amount: amount
							,	invoice: invoice.get('internalid')
							,	refnum: invoice.get('tranid')
							});

							deposits_total += amount;

							invoice.set('remaining', invoice.get('remaining') - amount);
							deposit.set('remaining', deposit.get('remaining') - amount);
						}
					});
				}
			});

			//Now try to apply credit memos
			credit_memos.each(function (credit_memo)
			{
				if (credit_memo.get('remaining') > 0)
				{
					invoices.each(function (invoice)
					{
						var amount = Math.min(invoice.get('remaining'), credit_memo.get('remaining'));

						if (amount > 0)
						{
							credit_memo.get('apply').push({
								amount: amount
							,	invoice: invoice.get('internalid')
							,	refnum: invoice.get('tranid')
							});

							creditmemos_total += amount;

							invoice.set('remaining',invoice.get('remaining') - amount);
							credit_memo.set('remaining',credit_memo.get('remaining') - amount);
						}
					});
				}
			});

			this.set('invoices_total', invoices_total);
			this.set('deposits_total', deposits_total);
			this.set('creditmemos_total', creditmemos_total);

			var payment = invoices_total - (deposits_total + creditmemos_total);
			this.set('payment', payment);
			this.set('payment_formatted', _.formatCurrency(payment));
			
			this.trigger('changeApply');
		}
	});
});
