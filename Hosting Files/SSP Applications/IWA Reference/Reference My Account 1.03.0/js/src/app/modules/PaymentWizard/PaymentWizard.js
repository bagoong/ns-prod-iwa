define('PaymentWizard', ['PaymentWizard.View', 'PaymentWizard.Step', 'PaymentWizard.Router'], function (View, Step, Router)
{
	'use strict';

	var config = [
		{
			name: _('SELECT INVOICES TO PAY').translate()
		,	steps: [{
				url: 'make-a-payment'
			,	hideBackButton: true
			,	hideContinueButton: true
			,	modules: [
					'PaymentWizard.Module.Invoice'
				,	['PaymentWizard.Module.Summary', { container: '#wizard-step-content-right' }]
				]
			,	save: function ()
				{
					return jQuery.Deferred().resolve();
				}
			}]
		}
	,	{
			name: _('PAYMENT REVIEW').translate()
		,	steps: [{
				url: 'review-payment'
			,	hideBackButton: true
			,	hideContinueButton: true
			,	modules: [
					'PaymentWizard.Module.ShowInvoices'
				,	'PaymentWizard.Module.ShowTotal'
				,	['PaymentWizard.Module.PaymentMethod.Creditcard', {title: _('Credit Card').translate()}]
				,	['PaymentWizard.Module.Addresses', {title: _('Billing Address').translate()}]
				,	['PaymentWizard.Module.Summary', { container: '#wizard-step-content-right', total_label: _('Payment Total').translate(), submit: true }]

				]
			,	save: function ()
				{
					return this.wizard.model.save();
				}
			}]
		}
	,	{
			name: _('Payment Confirmation').translate()
		,	steps: [{
				url: 'payment-confirmation'
			,	hideBackButton: true
			,	hideBreadcrumb: true
			,	hideContinueButton: true
			,	modules: [
					'PaymentWizard.Module.Confirmation'
				,	'PaymentWizard.Module.ShowInvoices'
				,	'PaymentWizard.Module.ShowTotal'
				,	'PaymentWizard.Module.ShowPayments'
				,	['PaymentWizard.Module.ConfirmationNavigation', { container: '#wizard-step-content-right', submit: true }]
				]
			}]
		}
	];

	return	{
		View: View
	,	Step: Step
	,	Router: Router

	,	mountToApp: function (application)
		{
			var Layout = application.getLayout();

			_.extend(Layout, {
				updateLayout: function()
				{
					this.updateHeader();
					this.updateFooter();
				}
			});
			_.extend(Layout.events, {
				'click [data-action="update-layout"]' : 'updateLayout'
			});

			return new Router(application, {
				profile: application.getUser()
			,	model: application.getLivePayment()
			,	steps: config
			});

		}
	};
});
