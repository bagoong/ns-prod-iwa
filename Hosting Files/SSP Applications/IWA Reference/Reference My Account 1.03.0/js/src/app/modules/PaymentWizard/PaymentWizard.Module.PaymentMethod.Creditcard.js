// PaymentWizard.Module.PaymentMethod.Creditcard.js
// --------------------------------
// 
define('PaymentWizard.Module.PaymentMethod.Creditcard', ['OrderWizard.Module.PaymentMethod.Creditcard'], function (OrderWizardModulePaymentMethodCreditcard)
{
	'use strict';

	return OrderWizardModulePaymentMethodCreditcard.extend({

			itemsPerRow: 3
		,	showDefaults: true
	});
});
