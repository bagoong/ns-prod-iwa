// PaymentWizard.Module.ShowPayments.js
// --------------------------------
// 
define('PaymentWizard.Module.ShowPayments', ['OrderWizard.Module.ShowPayments'], function (OrderWizardModuleShowPayments)
{
	'use strict';

	return OrderWizardModuleShowPayments.extend({

		totalChange: jQuery.noop
	});
});