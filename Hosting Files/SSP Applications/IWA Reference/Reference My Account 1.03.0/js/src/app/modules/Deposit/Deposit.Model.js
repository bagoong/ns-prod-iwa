define('Deposit.Model',['OrderPaymentmethod.Collection', 'Invoice.Collection'], function (OrderPaymentmethodCollection, InvoiceCollection)
{
	'use strict';

	function validateAmountRemaining(value, name, form)
	{
		if (isNaN(parseFloat(value)))
		{
			return _('The amount to apply is not a valid number').translate();
		}
		if (value <= 0)
		{
			return _('The amount to apply has to be positive').translate();
		}
		if (value > form.amountremaining_original)
		{
			return _('The amount to apply cannot exceed the remaining').translate();
		}
	}

	return Backbone.Model.extend({

			urlRoot: 'services/deposit.ss'

		,	validation : {
				amountremaining: { fn: validateAmountRemaining }
			}

		,	initialize: function (attributes)
			{
				this.on('change:paymentmethods', function (model, paymentmethods)
				{
					model.set('paymentmethods', new OrderPaymentmethodCollection(paymentmethods), {silent: true});
				});

				this.trigger('change:paymentmethods', this, attributes && attributes.paymentmethod || []);

				this.on('change:invoices', function (model, invoices)
				{
					model.set('invoices', new InvoiceCollection(invoices), { silent: true });
				});

				this.trigger('change:invoices', this, attributes && attributes.invoices || []);
			}
	});
});