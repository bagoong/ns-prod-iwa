// ListHeader:
// View used to manipulate a collection
// by adding sorting and filtering capabilities
// based on the sort and filter options from the collection
define('ListHeader',  function ()
{
	'use strict';

	return Backbone.View.extend({

		template: 'list_header'

	,	events: {
			'change [data-action="filter"]': 'filterHandler'
		,	'change [data-action="sort"]': 'sortHandler'
		,	'click [data-action="toggle-sort"]': 'toggleSortHandler'
		,	'change [data-action="select-all"]': 'selectAll'
		,	'change [data-action="unselect-all"]': 'unselectAll'
		,	'change [data-action="range-filter"]': 'rangeFilterHandler'
		}

	,	initialize: function (options)
		{
			var self = this
			,	view = options.view
			,	collection = options.collection;

			this.application = options.application;
			this.view = view;
			this.collection = collection;
			this.selectable = options.selectable;
			this.rangeFilter = options.rangeFilter;
			this.notUseDefaultDateRange = options.notUseDefaultDateRange;
			this.quantityDaysRange = options.quantityDaysRange;

			// store the filters from the collection
			this.filters = view.filterOptions;
			// store the sorts from the collection
			this.sorts = view.sortOptions;
			//store the range (date) filter options
			this.rangeFilterOptions = view.rangeFilterOptions || {};

			// after the parent view is rendered
			view.on('afterViewRender', function ()
			{
				var $place_holder = view.$el.find('[data-type="list-header-placeholder"]');
				// we render the ListHeader view
				self.render();

				// prepend it to the parent
				self.$el.prependTo($place_holder.length ? $place_holder : view.$el);
				// and add the event listeners
				self.delegateEvents();
			});
		}

		//returns the initial date range to apply
	,	getInitialDateRange: function (url_range)
		{
			if (this.rangeFilter)
			{
				var date_range_fromUrl = this.getRangeFromUrl(url_range);
				if (date_range_fromUrl.from || date_range_fromUrl.to)
				{
					return date_range_fromUrl;
				}
				else
				{
					var quantityDays = this.notUseDefaultDateRange ?
										this.quantityDaysRange :
										this.application.getConfig('filterRangeQuantityDays');

					if (quantityDays) {
						var from = new Date()
						,	to =  new Date();

						from.setDate(from.getDate() - quantityDays);
						
						return {
							from: _.dateToString(from)
						,	to: _.dateToString(to)
						};
					}
				}
			}
		}

		// when rendering we need to check
		// if there are options already set up in the url
	,	render: function ()
		{
			if (!this.selectedFilter && !this.selectedSort && !this.order && !this.selectedRange)
			{
				var url_options = _.parseUrlOptions(Backbone.history.fragment);

				this.selectedFilter = this.filters && this.getFilterFromUrl(url_options.filter);
				this.selectedRange = this.getInitialDateRange(url_options.range);
				this.selectedSort = this.sorts && this.getSortFromUrl(url_options.sort);
				this.order = this.getOrderFromUrl(url_options.order);

				// after we set the current status
				// we update the collection
				this.updateCollection();
			}

			return this._render();
		}

		// updateCollection:
		// the collection used by the view MUST have an update method
		// this method is going to be called whenever a sort/filter value changes
	,	updateCollection: function ()
		{
			var range;
			if (this.selectedRange) {
				range = {
					from: this.selectedRange.from
				,	to: this.selectedRange.to
				};
			}

			this.collection.update({
				filter: this.selectedFilter
			,	range : range
			,	sort: this.selectedSort
			,	order: this.order
			});

			return this;
		}

		// returns a specific filter
	,	getFilter: function (value)
		{
			return _.findWhere(this.filters, {
				value: value
			});
		}

		// returns a specific sort
	,	getSort: function (value)
		{
			return _.findWhere(this.sorts, {
				value: value
			});
		}

		// retuns the selected filter from the url
		// or the default filter if no value set up
	,	getFilterFromUrl: function (url_value)
		{
			var url_selected_filter = this.getFilter(url_value);

			if (url_selected_filter)
			{
				return url_selected_filter;
			}

			return this.getDefaultFilter();
		}

	,	getRangeFromUrl: function (url_value)
		{
			var split = url_value ? url_value.split('to') : [];

			return {
				from: split[0]
			,	to: split[1]
			};
		}

		// returns the selected sort from the url
		// or the default sort if no value set up
	,	getSortFromUrl: function (url_value)
		{
			var url_selected_sort = this.getSort(url_value);

			if (url_selected_sort)
			{
				return url_selected_sort;
			}

			return this.getDefaultSort();
		}

		// returns the selected order from the url
		// this could be inverse or nothing
	,	getOrderFromUrl: function (url_value)
		{
			return url_value === 'inverse' ? -1 : 1;
		}

		// if there's already a default filter, return that
		// otherwise find the one selected on the filter list
	,	getDefaultFilter: function ()
		{
			return this.defaultFilter || (this.defaultFilter = _.findWhere(this.filters, {selected: true}) || _.first(this.filters));
		}

		// if there's already a default sort, return that
		// otherwise find the one selected on the sort list
	,	getDefaultSort: function ()
		{
			return this.defaultSort || (this.defaultSort = _.findWhere(this.sorts, {selected: true}) || _.first(this.sorts));
		}

	,	isDefaultFilter: function (filter)
		{
			return this.getDefaultFilter() === filter;
		}

	,	isDefaultSort: function (sort)
		{
			return this.getDefaultSort() === sort;
		}

		// method called when dom dropdown change
	,	filterHandler: function (e)
		{
			// unselect all elements
			this.unselectAll({
				silent: true
			});
			// sets the selected filter
			this.selectedFilter = this.getFilter(e.target.value);
			// updates the url and the collection
			this.updateUrl().updateCollection();
		}

		// method called when dom dropdown change
	,	sortHandler: function (e)
		{
			// sets the selected sort
			this.selectedSort = this.getSort(e.target.value);
			// updates the url and the collection
			this.updateUrl().updateCollection();
		}

		// method called when dom button clicked
	,	toggleSortHandler: function ()
		{
			// toggles the selected order
			this.order *= -1;
			// updates the url and the collection
			this.updateUrl().updateCollection();
		}

		// selects all in collection
	,	selectAll: function ()
		{
			if ('selectAll' in this.view)
			{
				this.view.selectAll();
			}

			return this;
		}

		// unselects in collection
	,	unselectAll: function (options)
		{
			if ('unselectAll' in this.view)
			{
				this.view.unselectAll(options);
			}

			return this;
		}

	,	rangeFilterHandler: _.throttle(function ()
		{
			var selected_range = this.selectedRange
			,	$ranges = this.$('[data-action="range-filter"]');

			$ranges.each(function ()
			{
				if (this.value)
				{
					selected_range[this.name] = this.value;
				}
				else
				{
					delete selected_range[this.name];
				}
			});
			
			this.validateDateRange(selected_range);

			// updates the url and the collection
			this.updateUrl().updateCollection();

			return this;
		}, 2500, {leading:false})

	,	validateDateRange: function (selected_range)
		{
			var from = new Date(selected_range.from)
			,	to = new Date(selected_range.to)
			,	toMin = new Date(this.rangeFilterOptions.toMin)
			,	toMax = new Date(this.rangeFilterOptions.toMax)
			,	fromMin = new Date(this.rangeFilterOptions.fromMin)
			,	fromMax = new Date(this.rangeFilterOptions.fromMax);

			if (this.rangeFilterOptions.toMin && _.isDateValid(toMin) && _.isDateValid(to) && to.getTime() < toMin.getTime())
			{
				selected_range.to = this.rangeFilterOptions.toMin;
			}
			else if (this.rangeFilterOptions.toMax && _.isDateValid(toMax) && _.isDateValid(to) && to.getTime() > toMax.getTime())
			{
				selected_range.to = this.rangeFilterOptions.toMax;
			}

			if (this.rangeFilterOptions.fromMin && _.isDateValid(fromMin) && _.isDateValid(from) && from.getTime() < fromMin.getTime())
			{
				selected_range.from = this.rangeFilterOptions.fromMin;
			}
			else if (this.rangeFilterOptions.fromMax && _.isDateValid(fromMax) && _.isDateValid(from) && from.getTime() > fromMax.getTime())
			{
				selected_range.from = this.rangeFilterOptions.fromMax;
			}
		}

	,	updateUrl: function ()
		{
			var url = Backbone.history.fragment;
			// if the selected filter is the default one
			//   remove the filter parameter
			// else change it for the selected value
			url = this.isDefaultFilter(this.selectedFilter) ? _.removeUrlParameter(url, 'filter') : _.setUrlParameter(url, 'filter', this.selectedFilter.value);
			// if the selected sort is the default one
			//   remove the sort parameter
			// else change it for the selected value
			url = this.isDefaultSort(this.selectedSort) ? _.removeUrlParameter(url, 'sort') : _.setUrlParameter(url, 'sort', this.selectedSort.value);
			// if the selected order is the default one
			//   remove the order parameter
			// else change it for the selected value
			url = this.order === 1 ? _.removeUrlParameter(url, 'order') : _.setUrlParameter(url, 'order', 'inverse');
			// if range from and range to are set up
			//   change them in the url
			// else remove the parameter
			if (this.selectedRange)
			{
				url = this.selectedRange.from && this.selectedRange.to ? _.setUrlParameter(url, 'range', this.selectedRange.from + 'to' + this.selectedRange.to) : _.removeUrlParameter(url, 'range');
			}

			// just go there already, but warn no one
			Backbone.history.navigate(url, {trigger: false});

			return this;
		}
	});
});
