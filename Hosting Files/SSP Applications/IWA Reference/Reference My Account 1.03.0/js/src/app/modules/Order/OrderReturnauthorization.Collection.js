// OrderReturnauthorization.Collection.js
// ------------------------------
// Order Return Authorizations collection
define('OrderReturnauthorization.Collection', ['OrderReturnauthorization.Model'], function (Model)
{
	'use strict';

	return Backbone.Collection.extend({
		model: Model
	});
});