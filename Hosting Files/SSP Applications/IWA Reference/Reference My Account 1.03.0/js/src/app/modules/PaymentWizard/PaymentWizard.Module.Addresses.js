define('PaymentWizard.Module.Addresses', ['OrderWizard.Module.Address.Billing'], function (OrderWizardModuleAddressBilling)
{
	'use strict';

	return OrderWizardModuleAddressBilling.extend({

		itemsPerRow: 3

   ,    getSelectedAddress: function ()
		{
			if (!this.addressId && !this.unset)
			{
				var default_address = this.addresses.findWhere({
					defaultbilling: 'T'
				});
				
				this.addressId = default_address && default_address.id;

				if (this.addressId)
				{
					this.model.set(this.manage, this.addressId);
				}
			}

			return this.addresses.get(this.addressId) || this.getEmptyAddress();
		}
	,   unsetAddress: function ()
		{
			this.unset = true;
			OrderWizardModuleAddressBilling.prototype.unsetAddress.apply(this, arguments);
		}
	});
});