define('Deposit', ['Deposit.Views', 'Deposit.Model', 'Deposit.Collection'], function (Views, Model, Collection)
{
	'use strict';

	return	{
		Views: Views
	,	Model: Model
	,	Collection: Collection
	
	,	mountToApp: function (application)
		{
			application.getUser().set('deposits', new Collection());
		}
	};
});
