// OrderItem.Collection.js
// -----------------------
// Collection of past ordered items
define('OrderItem.Collection', ['OrderItem.Model'], function (Model)
{
	'use strict';

	return Backbone.CachedCollection.extend({
		
		url: 'services/orderitems.ss'
		
	,	model: Model
		
	,	parse: function (response)
		{
			this.totalRecordsFound = response.totalRecordsFound;
			this.recordsPerPage = response.recordsPerPage;

			return response.records;
		}
	});
});