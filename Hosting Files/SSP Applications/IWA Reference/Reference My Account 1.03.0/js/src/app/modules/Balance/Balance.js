define('Balance', ['Balance.View', 'Balance.Router'], function (View, Router)
{
	'use strict';

	return	{
		View: View
	,	Router: Router

	,	MenuItems: {
			name: _('Billing').translate()
		,	id: 'billing'
		,	index: 3
		,	children: [
				{
					id: 'balance'
				,	name: _('Account Balance').translate()
				,	url: 'balance'
				,	index: 1
				}
			,	{
					id: 'invoices'
				,	name: _('Invoices').translate()
				,	url: 'invoices'
				,	index: 2
				,	permission: 'transactions.tranCustInvc.1'
				}
			,	{
					id: 'transactionhistory'
				,	name: _('Transaction History').translate()
				,	url: 'transactionhistory'
				,	permissionOperator: 'OR'
				,	permission: 'transactions.tranCustInvc.1, transactions.tranCustCred.1, transactions.tranCustPymt.1, transactions.tranCustDep.1, transactions.tranDepAppl.1'
				,	index: 3
				}
			,	{
					id: 'printstatement'
				,	name: _('Print a Statement').translate()
				,	url: 'printstatement'
				,	index: 4
				,	permission: 'transactions.tranStatement.2'
				}
			]
		}

	,	mountToApp: function (application)
		{
			var Layout = application.getLayout();

			_.extend(Layout,
			{
				itemsExpander: function(e)
				{
					e.preventDefault();
					var $accordion = jQuery(e.currentTarget).parent();
					$accordion.find('[data-action="items-expander"] a i').toggleClass('icon-minus');
					$accordion.find('[data-content="items-body"]').stop().slideToggle();
				}
			});

			_.extend(Layout.events,
			{
				'click [data-action="items-expander"]' : 'itemsExpander'
			});

			return new Router(application);
		}
	};
});
