// OrderItem.Views.js
// -----------------------
// Views for ordered items
define('OrderItem.Views', ['ItemDetails.Model'], function (ItemDetailsModel)
{
	'use strict';

	var Views = {};

	Views.ReorderList = Backbone.View.extend({
		template: 'reorder_items'
	,	title: _('Reorder Items').translate()
	,	page_header: _('Reorder Items').translate()
	,	attributes: {'class': 'OrderItemReorderListView'}
	,	events: {
			'submit form': 'orderItems'
		,	'change #sortBy': 'sortByNavigation'
		,	'change [data-toggle="quantity"]' : 'setQuantity'
		}

		// change the view's sorting
	,	sortByNavigation: function (e)
		{
			Backbone.history.navigate(this.$(e.target).val(), {
				trigger: true
			});
		}

	,	initialize: function (options)
		{
			if (options.order_id)
			{
				this.basePath = 'reorderItems/order/' + options.order_id + '/';
			}
			else
			{
				this.basePath = 'reorderItems/';
			}
		}

	,	setQuantity: function (e)
		{
			var $input = jQuery(e.target)
			,	quantity = $input.val()
			,	line_id = $input.data('line-id');

			this.collection.get(line_id).get('item').set('quantity', quantity);
			
			this.showContent();
		}

	,	showContent: function ()
		{	
			//only render when we are actually in reorderitems web page for preventing re-rendering when navigation to other page when reorderitem ajax still loading
			if (Backbone.history.getHash().indexOf('reorderItems') === -1)
			{
				return;
			}

			var crumbtrail = [{
				text: this.title
			,	href: '/reorderItems'
			}];

			if (this.options.order_id && this.collection.at(0) && this.collection.at(0).get('order_number'))
			{
				var order_number = this.collection.at(0).get('order_number');
				this.title = _('Reorder Items from Order #$(0)').translate(order_number);
				crumbtrail.push({text: _('Order #$(0)').translate(order_number), href: '/reorderItems/order/' + this.options.order_id});
			}
			var dont_scroll = true;
			this.options.application.getLayout().showContent(this, 'reorderitems', crumbtrail, dont_scroll);
		}

		// reorder item, the quantity is written by the user on the input and the options are the same that the ordered item in the previous order
	,	orderItems: function (e)
		{
			e.preventDefault();
			
			var	application = this.options.application
			,	$form = this.$(e.target)
			,	$link = $form.find('input[name=item_id]')
			,	$quant = $form.find('input[name=item_quantity]')
			
			,	itemToCart = new ItemDetailsModel({
					internalid: $link.data('item-id')
				,	quantity: parseInt($quant.val(), 10) || 1
				,	options: $link.data('item-options') || null
				});
				
			if (parseInt(itemToCart.get('quantity'), 10) > 0 || isNaN(itemToCart.get('quantity')))
			{

				if (isNaN(itemToCart.get('quantity')))
				{
					itemToCart.set('quantity', 1);
				}
				application.getCart().addItem(itemToCart, {
					success: function ()
					{
						jQuery('p.success-message').remove();
						var $success = jQuery('<p/>');
						
						if (itemToCart.get('quantity') > 1)
						{
							$success.
								addClass('success-message pull-right')
								.html(itemToCart.get('quantity') + _(' items successfully added to <a href="#" data-touchpoint="viewcart">your cart</a></br>').translate())
								.appendTo($form);
						}
						else
						{
							$success.
								addClass('success-message pull-right')
								.html(_('Item successfully added to <a href="#" data-touchpoint="viewcart">your cart</a></br>').translate())
								.appendTo($form);
						}

						setTimeout(function ()
						{
							$success.fadeOut(function ()
							{
								$success.remove();
							});
						}, 6000);
					}
				});
			}
			else
			{
				jQuery('p.success-message').remove();
				var $msg = jQuery('<p/>');
				$msg.
					addClass('success-message pull-right')
					.html(_('The number of items must be positive.').translate())
					.appendTo($form);
			}
		}
	});

	return Views;
});
