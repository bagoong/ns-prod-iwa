// OrderReturnauthorization.Model.js
// -------------------------
// Return Authorizations model
define('OrderReturnauthorization.Model',['OrderLine.Collection'], function (OrderLinesCollection)
{
	'use strict';

	return Backbone.Model.extend({
		initialize: function (attributes)
		{
			this.on('change:lines', function (model, lines)
			{
				model.set('lines', new OrderLinesCollection(lines), {silent: true});
			});
			this.trigger('change:lines', this, attributes && attributes.lines || []);	
		}
	});

	
});