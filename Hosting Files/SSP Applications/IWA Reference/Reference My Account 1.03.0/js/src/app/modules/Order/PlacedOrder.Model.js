// PlacedOrder.Model.js
// -----------------------
// Model for showing information about past orders
define('PlacedOrder.Model', ['Order.Model','OrderFulfillment.Collection','OrderReturnauthorization.Collection', 'Receipt.Collection'], function (OrderModel,OrderFulfillmentsCollection,OrderReturnauthorizationCollection,ReceiptCollection)
{
	'use strict';

	var Model = OrderModel.extend({
		
		urlRoot: 'services/placed-order.ss'
		
	,	initialize: function (attributes)
		{

			// call the initialize of the parent object, equivalent to super()
			OrderModel.prototype.initialize.apply(this, arguments);
			
			this.on('change:fulfillments', function (model, fulfillments)
			{
				model.set('fulfillments', new OrderFulfillmentsCollection(fulfillments), {silent: true});
			});
			this.trigger('change:fulfillments', this, attributes && attributes.fulfillments || []);
			
			this.on('change:receipts', function (model, receipts)
			{
				model.set('receipts', new ReceiptCollection(receipts), {silent: true});
			});
			this.trigger('change:receipts', this, attributes && attributes.receipts || []);

			this.on('change:returnauthorizations', function (model, returnauthorizations)
			{
				model.set('returnauthorizations', new OrderReturnauthorizationCollection(returnauthorizations), {silent: true});
			});
			this.trigger('change:returnauthorizations', this, attributes && attributes.returnauthorizations || []);
		}
	});

	// add support for cache
	Model.prototype.sync =  Backbone.CachedModel.prototype.sync;
	Model.prototype.addToCache =  Backbone.CachedModel.prototype.addToCache;

	return Model;
});
