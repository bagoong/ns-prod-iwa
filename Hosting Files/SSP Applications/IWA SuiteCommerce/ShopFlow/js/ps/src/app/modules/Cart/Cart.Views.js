// Cart.Views.js
// -------------
// Cart and Cart Confirmation views
define('CustomCart.Views', ['Cart.Views'], function (CartViews) {

	'use strict';

	_.extend(CartViews.Detailed.prototype, {
		
		events: _.extend(CartViews.Detailed.prototype.events,
		{
            'click input[name="delivery-options"]': 'changeDeliveryOptions'
		})
		
	});
	
    CartViews.Detailed.prototype.changeDeliveryOptions = function(e) {
        e.preventDefault();
        var shipmethod = this.$(e.target).val(),
            self = this;
        this.model.set('shipmethod', shipmethod);
        this.model.save().always(function()
        {
            self.showContent();
        });
    }
	
});
