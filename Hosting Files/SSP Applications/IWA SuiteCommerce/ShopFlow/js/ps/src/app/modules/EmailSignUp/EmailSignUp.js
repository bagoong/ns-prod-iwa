define('EmailSignUp',function(){

    var Model = Backbone.Model.extend({
        url: _.getAbsoluteUrl('services/email-sign-up.ss')
    });

    return{
        mountToApp: function(application){
            var Layout = application.getLayout();
            var successText = '<div style="padding: 20px;"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; right: 10px; top:10px;background-color: white;border-radius: 50%;opacity: 1;border: solid 1px black;overflow: hidden;background: white;color: #222;padding: 1px;"> × </button> <h1 style="color: #961b21; font-size: 33px; text-align: center;font-weight: 100; margin-top: 35px;">THANK YOU FOR JOINING!</h1> <p style="color: #222; font-size: 21px; text-align: center; font-weight: 100; margin-top: 10px; line-height: 1.1;"> Look for exclusive promotions, exciting new products, <br>and oenophile articles from IWA. </p><input type="button" value="START SHOPPING " data-dismiss="modal" class="btn btn-primary" style="text-align: center; display: block; margin: auto;"/></div>';
            _.extend(Layout,{
                events: _.extend(Layout.events,{
                    'click #in-modal-mc-embedded-subscribe':'emailSignUp'
                }),
                emailSignUp: function(e){
                    e.preventDefault();
                    var $form = jQuery(e.target),
                        email = _.escape(jQuery('#in-modal-mce-EMAIL1').val()),

                        config = application.getConfig('emailSignUp') || {},
                        response;

                    if (!_.isNull(email)) {
                        var model = new Model();
                        model.save(_.extend(config, {
                            email: email
                        }), {
                            success: function (model, r) {
                                response = SC.macros.message(r.successMessage, 'success', false);
                                jQuery('.modal-body').html(successText);
                            },
                            error: function (model, r) {
                                response =  SC.macros.message(r.responseText, 'error', true);
                                jQuery('.formSignUp').find('.alert-placeholder').html(response);
                            }
                        });
                    } else {
                        response = SC.macros.message(isValidEmail, 'error', true);
                        $form.find('.alert-placeholder').html(response);
                    }

                }
            })
        }
    }
});