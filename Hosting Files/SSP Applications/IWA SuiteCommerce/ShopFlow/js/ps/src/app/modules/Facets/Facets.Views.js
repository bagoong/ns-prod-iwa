define('Facets.Views.Ext', ['Facets.Views', 'Categories'],
	function(Views, Categories){

		'use strict';

		_.extend(Views.Browse.prototype, {

			events: _.extend(Views.Browse.prototype.events, {
				'change [data-toggle="dropdown-facet"]': 'facetDropdown'
			}),

			initialize: function (options)
			{
				this.statuses = statuses;
				this.translator = options.translator;
				this.application = options.application;
				var categories = Categories.getBranchLineFromPath(this.translator.getFacetValue('category')) 
				this.category = categories[categories.length-1];
			},

			getDescription: function() {
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var category_description = category.storedetaileddescription || category.itemid;
						if(category_description)
						{
							return category_description; 
						}
					}
				}
				
				return '';
			}

			,	getCanonical: function ()
				{
					var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category'))
					if(categories && categories.length > 0) {
						var ret = categories[categories.length - 1].url;

						ret = ret.replace('undefined', window.location.protocol + '//' + window.location.hostname);

						return ret;
					}

					return this.getPath();
				}

			, getMetaDescription: function() {
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var meta = jQuery(category.metataghtml).attr('content');
						if (meta) return meta;
					}
				}
				return '';
			},

			getName: function() {
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var category_name = category.itemid;
						if(category_name)
						{
							return category_name; 
						}
					}
				}
				
				return '';
			},


			facetDropdown: function(e) {
				var self = this
				,	$target = jQuery(e.target);

				var option = $target.find('option:selected'),
					facethref = option.data('href');

				Backbone.history.navigate(facethref, {trigger: true});

				
			},

			_getBreadcrumb: Views.Browse.prototype.getBreadcrumb,

			getBreadcrumb: function() {

				var category_string = this.translator.getFacetValue('category')
					,	breadcrumb = [{
						href: '/'
					,	text: _('Home').translate()
					}];

				if (category_string)
				{
					var category_path = '';
					_.each(Categories.getBranchLineFromPath(category_string), function (cat)
					{
						category_path += '/'+cat.urlcomponent;
						
						breadcrumb.push({
							href: category_path + '/'
						,	text: _(cat.itemid).translate()
						});
					});
					
				}
				else if (this.translator.getOptionValue('keywords'))
				{
					breadcrumb.push({
						href: '#'
					,	text: _('Search Results').translate()
					});
				}
				else
				{
					breadcrumb.push({
						href: '#'
					,	text: _('Shop').translate()
					});
				}
				
				return breadcrumb;
			}
		});

		_.extend(Views.BrowseCategories.prototype, {
      		id: 'category-container',

			getDescription: function() {
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var category_description = category.storedetaileddescription || category.itemid;
						if(category_description)
						{
							return category_description; 
						}
					}
				}
				
				return '';
			}

			, getMetaDescription: function() {
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var meta = jQuery(category.metataghtml).attr('content');
						if (meta) return meta;
					}
				}
				return '';
			},

			getName: function() {

				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var category_name = category.itemid;
						if(category_name)
						{
							return category_name; 
						}
					}
				}
				
				return '';
			},

			getImage: function() {

				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category')); 
				if(categories && categories.length > 0)
				{
					for(var i = categories.length - 1; i >= 0; i--)
					{
						var category = categories[i]; 
						var storedisplayimage = category.storedisplayimage;
						if(storedisplayimage)
						{
							return storedisplayimage; 
						}
					}
				}
				
				return '';
			},

			initialize: function ()
			{
				var self = this;
				var categories = Categories.getBranchLineFromPath(this.options.translator.getFacetValue('category'));
				this.category = categories[categories.length-1];
				var urlComponent = '';
				for (var i = 0; i < categories.length-1;i++) {
					urlComponent += categories[i].urlcomponent + '/';
				}

				urlComponent += this.category.urlcomponent;

				this.translator = this.options.translator;

				this.hasThirdLevelCategories = false;

				this.facets = [];

				if (this.hasThirdLevelCategories)
				{
					_.each(this.category.categories, function (sub_category)
					{
						var facet = {
							configuration: {
								behavior: 'single'
							,	id: 'category'
							,	name: sub_category.itemid
							,	uncollapsible: true
							,	url: '/' + urlComponent + '/' + sub_category.urlcomponent + '/'			
							}
						,	values: {
								id: 'category' 
							,	values: []
							}
						};
						_.each(sub_category.categories, function (third_level_category)
						{
							var url = '/' + urlComponent + '/' + sub_category.urlcomponent + '/' + third_level_category.urlcomponent + '/';

							facet.values.values.push({
								label: third_level_category.itemid
							,	url: url
							,	image: third_level_category.storedisplaythumbnail
							});
						});

						self.facets.push(facet);
					});
				}
				else
				{
					var facet = {
						configuration: {
							behavior: 'single'
						,	id: 'category'
						,	name: ''
						,	uncollapsible: true
						,	hideHeading: true
						}
					,	values: {
							id: 'category' 
						,	values: []
						}
					};
					
					_.each(this.category.categories, function (sub_category)
					{
						var url = '/' + urlComponent + '/' + sub_category.urlcomponent + '/';

						facet.values.values.push({
							label: sub_category.itemid
						,	url: url
						,	image: sub_category.storedisplaythumbnail
						});
					});

					this.facets.push(facet);
				}
			},

			getBreadcrumb: Views.Browse.prototype.getBreadcrumb


		});

  	return Views;

});
