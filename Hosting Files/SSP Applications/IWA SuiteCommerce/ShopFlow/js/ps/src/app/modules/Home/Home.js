// Views.CustomHomeView.js
// ---------------
// View that customizes Facet Browse properties

define('Home.Ext',['Home'], function(Home) {

  'use strict';

  var $ = jQuery,
      HomeViewProto = Home.View.prototype;

  _.extend(HomeViewProto, {

    events: _.extend(HomeViewProto.events, {
      'click .share-facebook': 'shareItemOnSocialNetwork'
    }),

    shareItemOnSocialNetwork: function (event) {
      var newWindow = window.open('about:blank','Share','location=1,toolbar=0,scrollbars=0,status=0,menubar=0,width=646,height=369');
      newWindow.location.href = event.currentTarget.href;

      return false;
    },

    setupMainSlider: function () {
      this.$el.find('#home-slider').bxSlider({
        responsive: true,
        controls: true,
        auto: true,
		pagerType: 'full',
        mode: 'fade',
        autoControls: true,
        pause: 8000,
        speed: 1000,
        slideWidth: 1053,
        preloadImages: 'visible',
        slideMargin: 0,
        autoControlsCombine: true,
        autoHover: true,
        pager: false,
        onSliderLoad: function(){
          $('div.bx-controls-direction').addClass('hidden-phone hidden-tablet');
        },
        buildPager: function(i) {
          return ' ';
        }
      });
    },

    setupTopSellingSlider: function () {
      this.$el.find('#home-top-selling .bxslider').bxSlider({
        responsive: false,
        mode: 'fade',
        controls: false,
        auto: true,
        pause: 5000,
        preloadImages: 'visible',
        slideMargin: 30,
        autoControlsCombine: true,
        autoHover: true,
        pager: false,
        minSlides: 1,
        maxSlides: 4,
        slideWidth: 270,
        moveSlides: 1,
        onSliderLoad: function () {
          $('div.bx-controls-direction').addClass('hidden-phone hidden-tablet');
        }
      });
    }	

  });

  return {
    mountToApp: function (application) {
      var Layout = application.getLayout();

      Layout.on('renderEnhancedPageContent', function (view, content) {
        if (content.target === '#home-slider') {
          view.setupMainSlider();
        }
      });
    }
  };

});