define('Remarketing',['Cart.Views','Home','ItemDetails.View','Facets.Views'], function (Cart,Home,ItemView,FacetsViews) {
 
 
    'use strict';
    return {
        trackHome: function () {
            var data = {
                pagetype: 'home'
            };
            this.activateRemarketingTag(data);
        },
        trackCart: function (cartLines) {
 
            var items = [],
                total = [0];
 
            _.each(cartLines, function (line) {
                items.push(line.get('item').get('internalid'));
                total[0] += parseFloat(line.get('total').toFixed(2));
            });
            var data = {
                pagetype: 'cart',
                prodid: items,
                totalvalue: total
            };
            this.activateRemarketingTag(data);
        },
        trackProduct: function (item) {
            var data = {
                pagetype: 'product',
                prodid: [item.get('internalid')],
                totalvalue: [item.get('_price').toFixed(2)]
            };
            this.activateRemarketingTag(data);
        },
        trackCategory: function () {
 
            var key = _.parseUrlOptions(Backbone.history.fragment).keywords || '';
            var data = {
                pagetype: key ? 'searchresults' : 'category'
            };
            this.activateRemarketingTag(data);
        },
        trackOther: function (v) {
            var data = {
                pagetype: 'other'
            };
            this.activateRemarketingTag(data);
        },
        activateRemarketingTag: function (data) {
 
            var tag_parameters = '';
            if (data.prodid) {
                tag_parameters += 'ecomm_prodid=' + data.prodid.join(',') + ';'
            }
            if (data.pagetype) {
                tag_parameters += 'ecomm_pagetype=' + data.pagetype + ';';
            }
            if (data.totalvalue) {
                tag_parameters += 'ecomm_totalvalue=' + data.totalvalue.join(',') + ';';
            }
 
            var pixelURL = "//googleads.g.doubleclick.net/pagead/viewthroughconversion/" + window.google_conversion_id + "/?value=0&guid=ON&script=0&data=" + encodeURIComponent(tag_parameters);
            var img = document.createElement("img");
            img.onload = function () {
                return;
            };
 
            img.src = pixelURL;
        },
        mountToApp: function (application) {
 
            if(SC.ENVIRONMENT.jsEnvironment === 'browser'){
                /* <![CDATA[ */ 
 
                window.google_conversion_id = 1009822754;
                window.google_custom_params = window.google_tag_params;
                window.google_remarketing_only = true;
                /* ]]> */
 
                var self = this,
                    Layout = application.getLayout();
 
                Layout.on('afterAppendView',function(v){
                    if(v instanceof Home.View){
                        self.trackHome();
                    }
                    else if(v instanceof Cart.Detailed){
                        self.trackCart(v.model.get('lines').models);
                    }
                    else if(v instanceof ItemView){
                        self.trackProduct(v.model);
                    }
                    else if(v instanceof FacetsViews.Browse || v instanceof FacetsViews.BrowseCategories){
                        self.trackCategory();
                    }
                    else{
                        self.trackOther(v);
                    }
 
                });
            }
 
        }
    }
 
});