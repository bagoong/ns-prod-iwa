// ItemDetails.Router.js
// ---------------------
// Adds route listener to display Product Detailed Page
// Parses any options pased as parameters	

define('ItemDetails.Router.Ext', ['ItemDetails.Router', 'ItemDetails.Model', 'ItemDetails.View', 'AdditionalInfo.Model', 'ItemDetails.Collection'], function(Router, Model, View, AdditionalInfo, Collection) {

  'use strict';

  _.extend(Router.prototype, {

    productDetails: function(api_query, base_url, options) {

      // Decodes url options 
      _.each(options, function(value, name) {
        options[name] = decodeURIComponent(value);
      });

      var application = this.application,
        model = new Model()
        // we create a new instance of the ProductDetailed View
        ,
        view = new View({
          model: model,
          baseUrl: base_url,
          application: this.application
        }),
        additionalInfo = new AdditionalInfo();

      model.fetch({
        data: api_query,
        killerId: this.application.killerId,
        success: function() {
          if (!model.isNew()) {
            if (api_query.id && model.get('urlcomponent') && SC.ENVIRONMENT.jsEnvironment === 'server') {
              nsglobal.statusCode = 301;
              nsglobal.location = model.get('_url');
            }

            // once the item is fully loadede we set its options
            model.parseQueryStringOptions(options);

            if (!(options && options.quantity)) {
              model.set('quantity', model.get('_minimumQuantity'));
            }

            additionalInfo.fetch({
              data: {
                itemid: model.id
              },
              success: function(response) {
                if(response && response.attributes) {
                  var attributes = response.attributes;
                  model.availlableFinishes = (attributes.finishes)? attributes.finishes : [];
                  model.giftWraps = (attributes.giftWrap)? attributes.giftWrap : [];
                }

                // we first prepare the view
                view.prepView();

                // then we show the content
                view.showContent();

              }
            })

          } else {
            // We just show the 404 page
            application.getLayout().notFound();
          }
        },
        error: function(model, jqXhr) {
          // this will stop the ErrorManagment module to process this error
          // as we are taking care of it 
          jqXhr.preventDefault = true;

          // We just show the 404 page
          application.getLayout().notFound();
        }
      });
    }

  })

});
