// GiftWrap.Model.js
// ---------------
// A Model that calls the getGiftWrapper service. To get the git graps of an item.
define('GiftWrap.Model', function () {
  'use strict';

  var original_fetch = Backbone.CachedModel.prototype.fetch;

  return Backbone.CachedModel.extend({

    urlRoot: SC.ENVIRONMENT.servicesBaseUrl + 'getGiftWrapper.ss',

    // model.fetch
    // -----------
    // We need to make sure that the cache is set to true, so we wrap it
    fetch: function (options) {
      options = options || {};
      return original_fetch.apply(this, arguments);
    }

  });
});
