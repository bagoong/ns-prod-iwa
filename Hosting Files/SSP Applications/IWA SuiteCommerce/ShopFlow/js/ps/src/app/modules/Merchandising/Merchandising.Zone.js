define('Merchandising.Zone.Extension', ['Merchandising.Zone'], function (MerchandisingZone) {

  'use strict';

  jQuery.fn.merchandisingZone = function (options) {
    return this.each(function () {
      jQuery(this).data('MerchandisingZone', new MerchandisingZone(this, options));	
    });
  };

  _.extend(MerchandisingZone.prototype, {

    initialize: function () {
      this.addLoadingClass();
      // the listeners MUST be added before the fetch ocurrs
      this.addListeners();

      // add the error handling
      var filters = this.getApiParams();
      // parses the merchandising rule filters into the filters obj
      _.each(this.model.get('filter'), function (rule_filter) {
        var values = rule_filter.field_value;
        var filter_value = '';
        for (var v in values) {
          if (typeof(values[v]) === "object") {
            for(var key in values[v]) {
              var value = values[v][key];
              filters[rule_filter.field_id + '.' + key] = value;
            }						
          } else {
            filter_value += values[v] + ',';	
          }					
        }

        if (filter_value) {
          filter_value = filter_value.substring(0, filter_value.length-1);
          filters[rule_filter.field_id] = filter_value;	
        }
      });

      this.items.fetch({
        cache: true,
        data: filters
      });
    }

  });

});
