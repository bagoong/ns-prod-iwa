// ItemDetails.View.js
// -------------------
// Handles the pdp and quick view

define('ItemDetails.View.Ext', ['ItemDetails.View', 'ItemDetails.Model', 'Facets.Translator', 'ItemDetails.Collection'], function (View, Model, FacetsTranslator, Collection) {

  var $ = jQuery,
      ViewPrototype = View.prototype;

	_.extend(ViewPrototype, {
		getCanonical: function() {
	    var customCanonical = this.model.get('custitem112');
	    if (customCanonical === '' || typeof customCanonical === 'undefined')
	    	return ViewPrototype.getCanonicalBackup();

	    // remove all slashes
	    customCanonical = customCanonical.replace('/', '');

	    return window.location.protocol + '//' + window.location.hostname + '/' + customCanonical;
		}

		, events: _.extend(ViewPrototype.events, {
			'change [data-type="gift-wrap"]': 'setGiftWrap',
			'change [name="option-custcol_gift_wrap_message"]': 'setGiftWrapMessage',
			'click button.disabled[data-type="add-to-cart"]' : 'chooseOptionReminder'
		})

		,	chooseOptionReminder : function (e) {
				alert("Please select the required options before adding to cart.");
			}

		,	setGiftWrap: function(e) {
			var giftWrapId = jQuery(e.target).val();
			this.model.setGiftWrap(giftWrapId);
			this.model.setOption('custcol_gift_wrap',giftWrapId);
			if (giftWrapId) {
				this.$('#gift-wrap-message').show();
			}
			else {
				this.$('#gift-wrap-message').hide();
			}
		}
		,	setGiftWrapMessage: function(e) {
			var giftWrapMessage = jQuery(e.target).val();
			this.model.setGiftWrapMessage(giftWrapMessage);

		}
		// view.addToCart:
		// Updates the Cart to include the current model
		// also takes care of updateing the cart if the current model is a cart item
		,	addToCart: function (e)
		{
			e.preventDefault();

			// Updates the quantity of the model
			this.model.setOption('quantity', this.$('[name="quantity"]').val());

			if (this.model.isReadyForCart())
			{

				var self = this
				,	cart = this.application.getCart()
				,	layout = this.application.getLayout()
				,	cart_promise
				,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate();

				if (this.model.itemOptions && this.model.itemOptions.GIFTCERTRECIPIENTEMAIL)
				{
					if (!Backbone.Validation.patterns.email.test(this.model.itemOptions.GIFTCERTRECIPIENTEMAIL.label))
					{
						self.showError(_('Recipient email is invalid').translate());
						return;
					}

				}

				if (this.model.cartItemId)
				{
					cart_promise = cart.updateItem(this.model.cartItemId, this.model).success(function ()
					{
						if (cart.getLatestAddition())
						{
							if (self.$containerModal)
							{
								self.$containerModal.modal('hide');
							}

							if (layout.currentView instanceof require('Cart').Views.Detailed)
							{
								layout.currentView.showContent();
							}
						}
						else
						{
							self.showError(error_message);
						}
					});
				}
				else
				{
					var items = [];

					this.model.itemOptions.custcol_gift_wrap_message = {};
					this.model.itemOptions.custcol_gift_wrap_message.internalid = this.model.getGiftWrapMessage();
					cart_promise = cart.addItem(this.model).success(function ()
					{
						if (cart.getLatestAddition())
						{
							layout.showCartConfirmation();
						}
						else
						{
							self.showError(error_message);
						}
					});

				}

			// Checks for rollback items.
				cart_promise.error(function (jqXhr)
				{
					var error_details = null;
					try {
						var response = JSON.parse(jqXhr.responseText);
						error_details = response.errorDetails;
					} finally {
						if (error_details && error_details.status === 'LINE_ROLLBACK')
						{
							var new_line_id = error_details.newLineId;
							self.model.cartItemId = new_line_id;
						}

						self.showError('We couldn\'t process your item');
					}
				});

				// disalbes the btn while it's being saved then enables it back again
				if (e && e.target)
				{
					var $target = jQuery(e.target).attr('disabled', true);

					cart_promise.always(function () {
						$target.attr('disabled', false);
					});
				}
			}
		},

	    afterAppend: _.wrap(ViewPrototype.afterAppend, function(fn) {
	      var $accordions = $('#product-detail-inner-box').find('.collapse:not(:first)');

	      if ($(window).width() < 768) {
	        $accordions.collapse('hide');
	        $accordions.parent().find('.accordion-toggle').addClass('collapsed');
	      }

	      fn.apply(this, _.toArray(arguments).slice(1));
	    }),

	    	getAvailableFinishes: function () {
	      	return new Collection(this.model.getAvailableFinishes()).models;
	    }

	});

	return {
    mountToApp: function (application) {
      var Layout = application.getLayout();

      Layout.on('afterRelatedItemsAppended', function () {
        var view = Layout.currentView,
            $relatedItemsAccordion = view.$('#accordion-group-6');

        if (!$relatedItemsAccordion.find('[data-type="related-items-placeholder"]').children().length) {
          $relatedItemsAccordion.hide();
        }
      });
    }
  };

});
