define('WelcomePopup', ['Home', 'EmailSignUp'], function (Home, EmailSignUp) {
    'use strict';
    var cookieName = 'welcomePopup',
        cookieDays = 1000,
        cookieDaysRemindmelater = 1;

    var Model = Backbone.Model.extend({
        url: _.getAbsoluteUrl('services/email-sign-up.ss')
    });

    var View = Backbone.View.extend({
        template: 'welcome_popup',
        title: _('Welcome Iwawine').translate(),
        page_header: _('Welcome to Iwawine').translate(),
        events:{
            "click #in-modal-notnow":"setCookieForLater",
            "click #in-modal-nothanks":"noThanks",
            "click .close-btn":"closePopup"
        },
        closePopup: function(){
            jQuery.cookie(cookieName, true, {expires: cookieDaysRemindmelater});
            jQuery(".welcome-modal").modal('hide')
        },
        setCookieForLater:function(){
            jQuery.cookie(cookieName, true, {expires: cookieDaysRemindmelater});
        },
        noThanks:function(){
            jQuery.cookie(cookieName, true, {expires: cookieDays});
        }
    });



    function openPopup() {
        if (jQuery.cookie(cookieName, undefined, {expires: cookieDays})) {
            return false;
        }
        else if (jQuery(window).width() <= 767) {
            return false;
        } else {
            jQuery.cookie(cookieName, true, {expires: cookieDays});
            return true;
        }

    }

    return {
        View: View,
        mountToApp: function (application) {

            var Layout = application.getLayout();

            //if (openPopup()) {
            if (typeof nsglobal === 'undefined' && openPopup()) {

                Layout.once('afterAppendView', function () {
                    setTimeout(function(){
                        var view = new View({application: application});
                        Layout.showInModal(view, {macro: 'emptyModal', className: 'welcome-modal'});
                        var $form = Layout.$('#in-modal-welcome-popup .newsletter-form form');
                        var settings = application.getConfig('newsletter');
                    },10000)

                    //$form.attr('action', settings.actionURL);
                    //$form.attr('method', 'post');
                });
            }

        }
    }
});