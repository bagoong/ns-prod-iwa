// Content.EnhancedViews.js
// ------------------------
// The Landing pages Module uses the Content.DataModels to connect to the servers,
// That's why there is only a view and a router here
define(
	'Content.LandingPage.Custom'
,	['Content']
,	function (Content)
{
	'use strict';

	// View:
	// Tho most of the content is driven by the content service 
	// we need a view to extend upon
	_.extend(Content.LandingPages.View.prototype, {
		generateCustomLanding: function (url, cpath) {
			if (cpath == null) {
			  	cpath = SC.ENVIRONMENT.landingsMenu;
			}
			var r = _.find(cpath, function(e) { return e.url == url} );
			if (r) {
				return [{
					href: r.url
				,	text: r.name
				}];
			}

			for (i in cpath) {
				var node = cpath[i];
				if (typeof node['children'] != 'undefined' && node.children.length > 0) {
					var nextBread = this.generateCustomLanding(url, node.children);

					if (typeof nextBread != 'undefined' && nextBread != null) {
						nextBread.unshift({href: node.url, text: node.name});
						return nextBread;
					}
				}
			}

			return null;
		},

		_getBreadcrumb: Content.LandingPages.View.prototype.getBreadcrumb,

		getBreadcrumb: function ()
		{
			var customBC = null;
			if (this.template == 'landing_page') {
				customBC = this.generateCustomLanding(this.url);
				if (customBC != null) {
					customBC.unshift({
						href: '/'
					,	text: _('Home').translate()
					});
					return customBC;
				}
			}

			return this._getBreadcrumb();
		}
	});
});