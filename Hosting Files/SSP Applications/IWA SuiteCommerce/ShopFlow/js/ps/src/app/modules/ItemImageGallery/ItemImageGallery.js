define('ItemImageGallery.Extension', ['ItemImageGallery'], function (ImageGalleryModule) {

	'use strict';

	_.extend(ImageGalleryModule.ItemImageGallery.prototype, {

    initZoom: function () {
      var self = this,
          images, $target;

      images = _.filter(this.images, function (image) {
        return _.isUndefined(image.video);
      });

      $target = _.filter(this.$target.children(), function (child) {
        return !jQuery(child).hasClass('video-thumb');
      });

      _.each($target, function (slide, slide_index) {
        //console.log('LOOP', images[slide_index]);
        jQuery(slide).zoom({
          url: ImageGalleryModule.resizeZoom(images[slide_index].url),
          callback: self.zoomImageCallback
        });
      });

      return this;
    },

    initSlider: function () {
      var promise = jQuery.Deferred();
      this.$target.data('item-slider-loaded', promise);

      return this.$target.bxSlider({
        buildPager: jQuery.proxy(this.buildSliderPager, this),
        startSlide: this.options.startSlide,
        forceStart: this.options.forceStart,
        adaptiveHeight: true,
        onSliderLoad: function () {
          promise.resolve();
        }
      });
    }

	});

});
