(function(application) {

  'use strict';

  application.on('beforeStartApp', function() {

    var $ = jQuery,
        configuration = application.Configuration,
        item_colors = {},
        listCheckbox = {
          'true': 'Yes',
          'false': 'No'
        };

    _.extend(item_colors, {
      'Black': 'black',
      'Blue': 'blue',
      'Cyan': 'cyan',
      'Green': 'green',
      'Magenta': 'magenta',
      'Red': 'red',
      'White': 'white'
    });

    configuration.vimeoImageNotAvailable = '/Images/css-images/vimeo_loading.jpg';

    var vimeoThumbnail = configuration.vimeoImageNotAvailable;

    var getIframeSrc = function(iframe) {
      return iframe.match(/^.*src="(.*?)".*/)[1];
    }

    var isYouTubeVideo = function(iframe) {
      return /\/\/(?:www\.)?youtube\./.test(iframe);
    }

    var getYouTubeHash = function(iframe) {
      return getIframeSrc(iframe).match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/)[7];
    }

    var isVimeoVideo = function(iframe) {
      return /\/\/(?:www\.)?player\.vimeo\.com\/video\//.test(iframe);
    }

    var getVimeoHash = function(iframe) {
      return getIframeSrc(iframe).match(/\/\/(?:www\.)?player\.vimeo\.com\/video\/(\d*)/)[1];
    }

    var addVideoThumbnail = function(iframe, images) {
      var vimeoUrl, url, altText;

      if (isYouTubeVideo(iframe)) {
        url = '//img.youtube.com/vi/' + getYouTubeHash(iframe) + '/hqdefault.jpg';
        altText = 'YouTube Video';
      } else if (isVimeoVideo(iframe)) {
        url = vimeoThumbnail;
        vimeoUrl = '//vimeo.com/api/v2/video/' + getVimeoHash(iframe) + '.json';
        $.getJSON(vimeoUrl, function(data) {
          url = data[0].thumbnail_small;
          $('ul.bxslider').data('item-slider-loaded').done(function() {
            $('.bx-pager-item').find('img[src^="' + vimeoThumbnail + '"]').attr('src', url);
          });
        });
        altText = 'Vimeo Video';
      }

      images.push({
        url: url,
        altimagetext: altText,
        video: iframe
      });
    }

    application.addModule('AvailableFinishes.Model');
    application.addModule('CustomCart.Views');
    application.addModule('Content.LandingPage.Custom');
    application.addModule('Facets.Translator.Custom');
    application.addModule('Facets.Views.Ext');
    application.addModule('LayoutShopFlow.Extension');
    application.addModule('GiftWrap.Model');
    application.addModule('Home.Ext');
    application.addModule('AdditionalInfo.Model');
    application.addModule('ItemDetails.Model.Ext');
    application.addModule('ItemDetails.Router.Ext');
    application.addModule('ItemDetails.View.Ext');
    application.addModule('ItemImageGallery.Extension');
    application.addModule('Merchandising.Zone.Extension');
    application.addModule('ItemRelations.Extension');
    application.addModule('Remarketing');
    application.addModule('EmailSignUp');
    application.addModule('WelcomePopup');


    _.extend(configuration, {
      emailSignUp :{
          h:"AAAXpM3fHxgDGftvQnc1sHlXAuYUE6NaiIw%3D",
          formid:95
      },
      searchApiMasterOptions: _.extend(configuration.searchApiMasterOptions, {

        Facets: {
          include: 'facets',
          fieldset: 'search',
          custitem38: '0'
        },

        itemDetails: {
          include: 'facets',
          fieldset: 'details',
          custitem38: '0'
        }

      }),

      facetsSeoLimits: {
        numberOfFacetsGroups: 1,
        numberOfFacetsValues: 0,
        options: ['page'] // order, page, show, display, keywords
      },

      // pagination
      defaultPaginationSettings: {
        showPageList: true,
        pagesToShow: 5,
        showPageIndicator: false
      },

      sortOptions: [{
        id: 'custitempopularity2:desc',
        name: _('Sort By').translate()
      }, {
        id: 'custitempopularity:desc',
        name: _('Best Selling').translate(),
        isDefault: true
      }, {
        id: 'relevance:desc',
        name: _('Relevance').translate()        
      }, {
        id: 'onlinecustomerprice:asc',
        name: _('Price, Low to High').translate()
      }, {
        id: 'onlinecustomerprice:desc',
        name: _('Price, High to Low').translate()
      }, {
        id: 'custitemiwa_item_number:desc',
        name: _('New').translate()
      }],

      resultsPerPage: [{
        items: 24,
        name: _('$(0) Items').translate('24')
      }, {
        items: 28,
        name: _('$(0) Items').translate('28'),
        isDefault: true
      }, {
        items: 100,
        name: _('$(0) Items').translate('100')
      }],

      typeahead: _.extend(configuration.typeahead, {
        sort: 'relevance:desc'
      }),

      // Analytics Settings
      // You need to set up both popertyID and domainName to make the default trackers work
      tracking: {
        trackPageview: true
        // [Google Universal Analytics](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
        ,
        googleUniversalAnalytics: {
          propertyID: 'UA-9999920-1',
          domainName: 'iwawine.com'
        }
      },

      addThis: {
        enable: true,
        pubId: 'ra-50abc2544eed5fa5',
        toolboxClass: 'addthis_default_style addthis_toolbox addthis_32x32_style',
        servicesToShow: {
          pinterest_share: '',
          facebook: '',
          email: '',
          google_plusone: '',
          expanded: ''
        }
      },

      facets: [{
        id: 'custitem65',
        name: _('Intended Use').translate(),
        url: 'intended-use',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 100
      }, {
        id: 'custitem71',
        name: _('Material').translate(),
        url: 'material',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 99
      }, {
        id: 'custitem70',
        name: _('Color').translate(),
        url: 'color',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 98
      }, {
        id: 'custitem84',
        name: _('Brand').translate(),
        url: 'brand',
        behavior: 'multi',
        uncollapsible: false,
        max: 5,
        priority: 97
      }, {
        id: 'custitem63',
        name: _('Collection').translate(),
        url: 'collection',
        behavior: 'multi',
        uncollapsible: false,
        max: 5,
        priority: 96
      }, {
        id: 'custitem64',
        name: _('Product Type').translate(),
        url: 'product-type',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 95
      }, {
        id: 'custitem66',
        name: _('Quality').translate(),
        url: 'quality',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 94
      }, {
        id: 'custitemstockstatus',
        name: _('Availability').translate(),
        url: 'availability',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 93
      }, {
        id: 'custitem_freeshipping',
        name: _('Free Shipping').translate(),
        url: 'freeshipping',
        macro: 'facetCheckbox',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 92
      }, {
        id: 'custitem33',
        name: _('Bulk Pricing').translate(),
        url: 'bulk',
        macro: 'facetCheckbox',
        behavior: 'multi',
        uncollapsible: false,
        max: 10,
        priority: 91
      },

        {
          id: 'custitem_round_average',
          name: _('Average Rating').translate(),
          url: 'review-average',
          uncollapsible: false,
          labels: {
            0: 'Not Rated'
          },
          macro: 'facetDropdown',
          priority: 90
        },

        {
          id: 'onlinecustomerprice',
          name: _('Price').translate(),
          url: 'price',
          behavior: 'range',
          macro: 'facetRange',
          uncollapsible: false,
          max: 10,
          priority: 89
        }, {
          id: 'custitem73',
          name: _('BTUH').translate(),
          url: 'btuh',
          behavior: 'multi',
          uncollapsible: false,
          max: 10,
          priority: 88
        }, {
          id: 'custitem76',
          name: _('Capacity').translate(),
          url: 'capacity',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 87
        }, {
          id: 'custitem67',
          name: _('Width').translate(),
          url: 'width',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 86
        }, {
          id: 'custitem68',
          name: _('Depth').translate(),
          url: 'depth',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 85
        }, {
          id: 'custitem69',
          name: _('Height').translate(),
          url: 'height',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 84
        }, {
          id: 'custitemcapacitybottles',
          name: _('Bottle Capacity').translate(),
          url: 'capacity-bottles',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 81
        }, {
          id: 'custitemcapacityvolume',
          name: _('Capacity').translate(),
          url: 'capacity-volume',
          behavior: 'range',
          // Steps of 1
          macro: 'facetRange',
          uncollapsible: false,
          priority: 80
        }, {
          id: 'custitem_featured_item',
          name: _('Featured Items').translate(),
          url: 'featured-items',
          uncollapsible: false,
          priority: 82
        }
      ],

      macros: _.extend(configuration.macros, {

        itemOptions: _.extend(configuration.macros.itemOptions, {

          selectorByType: {
            select: 'itemDetailsOptionTileCustom',
            'default': 'itemDetailsOptionText'
          }

        })

      }),

      itemOptions: [{
        cartOptionId: 'custcol_is_gift_wrap',
        itemOptionId: 'custitem_is_gift_wrap',
        macros: {
          selector: 'itemDetailsOptionHidden',
          selected: 'shoppingCartOptionHidden'
        }
      }, {
        cartOptionId: 'custcol_gift_wrap',
        itemOptionId: 'custitem_gift_wrap',
        macros: {
          selected: 'shoppingCartOptionHidden',
          selector: 'itemOptionGiftWrap'
        }
      }, {
        cartOptionId: 'custcol_gift_wrap_package',
        itemOptionId: 'custitem_gift_wrap_package',
        macros: {
          selector: 'itemDetailsOptionHidden',
          selected: 'shoppingCartOptionHidden'
        }
      }, {
        cartOptionId: 'custcol45',
        itemOptionId: 'custitem56'
      }, {
        cartOptionId: 'custcolwci_wood',
        itemOptionId: 'custitem57'
      }, {
        cartOptionId: 'custcol49',
        itemOptionId: 'custitem59'
      }, {
        cartOptionId: 'custcol50',
        itemOptionId: 'custitem58'
      }, {
        cartOptionId: 'custcol_selectassemblyitem',
        itemOptionId: 'custitem_relatedassemblies',
        macros: {
          selector: 'itemDetailsOptionHidden',
          selected: 'shoppingCartOptionAssemblies'
        }
      }, {
        cartOptionId: 'custcol42',
        itemOptionId: 'custitem42',
        macros: {
          selected: 'shoppingCartOptionHidden'
        }
      }, {

        cartOptionId: 'custcol_vent_option',
        itemOptionId: 'custitem_vent_option'
      }, {
        cartOptionId: 'custcol_cooling_unit',
        itemOptionId: 'custitem_cooling_unit'
      }],

      productReviews: _.extend(configuration.productReviews, {

        reviewMacro: 'showReviewCustom'

      })

    });

  });

  SC.Translations['Add to my product lists'] = 'Add to My Wish Lists';
  SC.Translations['Add to my product list'] = 'Add to My Wish List';

  Backbone.View.prototype.getCanonicalBackup = function() {
    // this is in order to fix different urls for the same item pointing at different canonical urls.
    var fragment = Backbone.history.fragment,
        canonical = window.location.protocol + '//' + window.location.hostname + '/';

    if (!fragment)
      return canonical;

    var fragment_index_period = fragment.indexOf('.'),
        fragment_index_quest = fragment.indexOf('?'),
        fragment_index = -1;

    if (fragment_index_period != -1) {
      fragment_index = fragment_index_period;
    } else if (fragment_index_quest != -1) {
      fragment_index = fragment_index_quest;
    }

    // !~ means: indexOf == -1
    return !~fragment_index ? canonical + fragment : canonical + fragment.substring(0, fragment_index);
  };

  Backbone.View.prototype.getCanonical = function() {
    return Backbone.View.prototype.getCanonicalBackup();
  };
}(SC.Application('Shopping')));