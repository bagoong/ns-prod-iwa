define('LayoutShopFlow.Extension', function () {

  'use strict';

  return {
    mountToApp: function (application) {
      var Layout = application.getLayout();

      Layout.on('afterAppendView', function (view) {
        if (!(view instanceof require('Home').View)) {
          Layout.$el.find('#home-slider').empty();
        }
      });
      _.extend(Layout, {
          events: _.extend(Layout.events, {
              'click .modal-header .close': 'closeModal'
          }),
          closeModal: function (event) {
               jQuery('.modal.fade.in').click();
          }
      });

    }
  }

});
