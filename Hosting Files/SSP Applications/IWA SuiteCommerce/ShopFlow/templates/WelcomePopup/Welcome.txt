<style>
    #modal-header{display: none;}
    @media only screen and (max-width: 769px) {
        .image-container-popup{
            width: 40%;
            float: left;
            background-image: url(http://iwawine.sandbox.netsuitestaging.com/Images/css-images/riedel-horn-popup.jpg);
            background-size: auto 100%;
            padding: 220px 0;
            background-repeat: no-repeat;
        }
        .image-container-popup img{display: none;}
        .form-container-popup input.email{width: 100%;}
    }
</style>
<div class="modal-body row" style="position: relative; padding: 0; margin: 0;">
    <div style="width: 60%; float: left;" class="form-container-popup">
        <div>
            <h1 style="color: #961b21; font-size: 33px; text-align: center;font-weight: 100; margin-top: 35px;">SIGN UP & SAVE</h1>
            <p style="color: #222; font-size: 21px; text-align: center; font-weight: 100; margin-top: 10px; line-height: 1.1;">Learn about Exclusive<br>
            Sales, Promotions<br>
            and New Arrivals
            </p>
        </div>
        <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate formSignUp" target="_blank" novalidate style="padding: 10px;">
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL1" placeholder="Enter Email Address" style="margin: 0;"/>
            <input type="button" value="SIGN UP" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary"/>
            <div class="alert-placeholder"></div>
        </form>
        <p><span data-dismiss="modal" style="text-align: center; display: block; text-decoration: underline; color: #961b21; cursor: pointer;">No thanks</span></p>
        <p style="font-size: 11px; text-align: center; margin-top: 25px;">We will NEVER share your email address. <a href="#" style="color:#961b21;">Read our Privacy Policy</a></p>
    </div>
    <div class="image-container-popup" style="width: 40%; float: left;">
        <img src="/Images/css-images/riedel-horn-popup.jpg" style="max-width: 100%;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; right: 10px; top:10px;background-color: white;border-radius: 50%;opacity: 1;border: solid 1px black;overflow: hidden;background: white;color: #222;padding: 1px;">
            ×
        </button>
    </div>
</div>

<!--
<div style="padding: 20px;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; right: 10px; top:10px;background-color: white;border-radius: 50%;opacity: 1;border: solid 1px black;overflow: hidden;background: white;color: #222;padding: 1px;">
        ×
    </button>
    <h1 style="color: #961b21; font-size: 33px; text-align: center;font-weight: 100; margin-top: 35px;">THANK YOU FOR JOINING!</h1>
    <p style="color: #222; font-size: 21px; text-align: center; font-weight: 100; margin-top: 10px; line-height: 1.1;">
        Look for exclusive promotions, exciting new products, <br>
        and oenophile articles from IWA.
    </p>
    <input type="button" value="START SHOPPING " data-dismiss="modal" class="btn btn-primary" style="text-align: center; display: block; margin: auto;"/>
</div>
-->