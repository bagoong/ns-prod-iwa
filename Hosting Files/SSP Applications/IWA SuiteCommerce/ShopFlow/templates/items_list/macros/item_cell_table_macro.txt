<% registerMacro('itemCellTable', function (item, view) { %>
<%
	var min_qty = parseInt(item.get('_minimumQuantity'), 10)
	,	thumbnail = item.get('_thumbnail')
	,	images = item.get('_images')
	,	macros = SC.macros;

	if (images && images.length) {
		thumbnail = images[0];
	}
%>
<div class="item-cell item-cell-table" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
	<meta itemprop="url" content="<%= item.get('_url') %>">
	<div class="item-cell-thumbnail thumbnail">
		<!-- Badges -->
		<% if(item.get('custitem_freeshipping')){ %>
			<div class="freeshipping"></div>
		<% } %>

		<% if(item.get('custitem_onsale')){ %>
			<div class="onsale"></div>
		<% } %>

		<% if(item.get('custitem1')){ %>
			<div class="newaccessories"></div>
		<% } %>

		<% if(item.get('custitem_bestselling')){ %>
        <div class="best-seller"></div>
    <% } %>

		<a href="<%= item.get('_url') %>">
			<img src="<%= view.options.application.resizeImage(thumbnail.url, 'main') %>" alt="<%= thumbnail.altimagetext %>" itemprop="image">
		</a>
		<% if (SC.ENVIRONMENT.jsEnvironment === 'browser') { %>
		<div class="btn-quick-view">
			<a href="<%= item.get('_url') %>" class="btn btn-primary btn-large" data-toggle="show-in-modal" data-modal-class-name="modal-big">
				<i class="icon-search icon-white"></i>
				<%= _('Quick View').translate() %>
			</a>
		</div>
		<% } %>
	</div>
	<h2 class="item-cell-name">
		<a href="<%= item.get('_url') %>">
			<span itemprop="name">
				<%= item.get('_name') %>
			</span>
		</a>
	</h2>
	<div class="item-cell-price">
		<!--<%
                  var basePrice = item.get('pricelevel1');
                  var basePriceFormatted = item.get('pricelevel1_formatted');
                  var onlinePrice_detail = item.get('onlinecustomerprice_detail');
                  var onlinePriceFormatted = onlinePrice_detail.onlinecustomerprice_formatted;
                  var onlinePrice = onlinePrice_detail.onlinecustomerprice;
              %>
              <%
                  if (basePrice > onlinePrice) { %>

                  <%= _('<span class="text-sub text-bold">$(0)</span>  <span class="price-red text-bold">$(1)</span>').translate(basePriceFormatted, onlinePriceFormatted) %>

              <% }else{ %>

                  <%= _('<span class="text-bold">$(0)</span>').translate(onlinePriceFormatted) %>

              <% } %>-->
              <%= macros.showItemPrice(item) %>
	</div>
	<div class="item-rating item-cell-rating rating-<%= item.get('_rating') %>" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<%= macros.starRating({
			max: view.options.application.getConfig('productReviews.maxRate')
		,	value: item.get('_rating')
		,	className: 'star pull-left'
		,	fillRounded: true
		}) %>
		<span class="review-total">
			(<span itemprop="reviewCount"><%= item.get('_ratingsCount')%></span>)
		</span>
	</div>
	<div class="options-container">
		<%= item && item.itemOptionsHelper && item.itemOptionsHelper.renderAllOptionSelector(
			_.where(item.itemOptionsHelper.getPosibleOptions(), {showSelectorInList: true})
		) %>
	</div>
	<% if (view.options.application.getConfig('addToCartFromFacetsView') && item.isReadyForCart()) { %>
		<form class="form-inline" data-toggle="add-to-cart">
			<input type="hidden" value="<%= item.get('_id') %>" name="item_id">
			<input name="quantity" class="input-mini" type="number" min="1" value="<%- min_qty %>">
			<input type="submit" class="btn btn-primary" value="Add to Cart">
		</form>
	<% } %>
	<div class="item-cell-stock">
		<%= macros.itemDetailsStock(item.getStockInfo()) %>
	</div>
</div>
<% }) %>
