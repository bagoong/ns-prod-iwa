Application.defineModel('Category', {
    
    get: function () {

        'use strict';

        var is_secure = request.getURL().indexOf('https') === 0,
            config = SC.projectConfig.site.categories,
            categories;

        if(!is_secure || config.secure_enable_subcategories) {
            categories = session.getSiteCategoryTree({ itemfields : ["pagetitle2","urlcomponent","canonicalurl","pagetitle","itemid","storeurl","storedetaileddescription","storedisplaythumbnail", "metataghtml"] });
        } else {
            categories = session.getSiteCategoryContents({ internalid: ''+config.home_id }, false);
        }

        return this.fixCategories(categories, '');
    },

    fixCategories: function (categories, url) {

        'use strict';

        var result = {},
            self = this;

        _.each(categories, function (category) {

            if(category.internalid > 0){
                var category_limited =  _.pick(category, 'itemid','pagetitle','urlcomponent','welcomepagetitle', 'categories', 'internalid', 'storedisplayimage', 'storedisplaythumbnail', 'storedetaileddescription', 'metataghtml');
                category_limited.urlcomponent = category_limited.urlcomponent || category_limited.itemid;
                category_limited.url = url + '/' + category_limited.urlcomponent;
                result[category_limited.urlcomponent] = category_limited;

                if (category_limited.categories && category_limited.categories.length) {
                    category_limited.categories = self.fixCategories(category_limited.categories, category_limited.url);
                }
            } else {
                result = self.fixCategories(category.categories);
            }

        });

        return result;
    }

});
