Application.defineModel = function(name, definition) {
    'use strict';
    Application.originalModels[name] = definition;
}

Application.extendModel = function (name, extensions)
{
    'use strict';

    if (Application.originalModels[name])
    {

        if (!Application.extendedModels[name])
        {
            Application.pushToExtendedModels(name);
        }

        var model = Application.extendedModels[name];
        _.each(extensions, function (value, key)
        {
            if (typeof value === 'function')
            {
                model[key] = Application.wrapFunctionWithEvents(name + '.' + key, model, value);
            } else {
                model[key] = value;
            }
        });


    }
    else
    {
        throw nlapiCreateError('APP_ERR_UNKNOWN_MODEL', 'The model ' + name + ' is not defined');
    }
}

Application.pushToExtendedModels = function (name)
{
    'use strict';

    var model = {};

    _.each(Application.originalModels[name], function (value, key)
    {
        if (typeof value === 'function')
        {
            model[key] = Application.wrapFunctionWithEvents(name + '.' + key, model, value);
        }
        else
        {
            model[key] = value;
        }
    });

    if (!model.validate)
    {
        model.validate = Application.wrapFunctionWithEvents(name + '.validate', model, function (data)
        {
            if (this.validation)
            {
                var validator = _.extend({
                        validation: this.validation
                        ,	attributes: data
                    }, Backbone.Validation.mixin)

                    ,	invalidAttributes = validator.validate();

                if (!validator.isValid())
                {
                    throw {
                        status: 400
                        ,	code: 'ERR_BAD_REQUEST'
                        ,	message: invalidAttributes
                    };
                }
            }
        });
    }

    Application.extendedModels[name] = model;
}

Application.getModel = function (name)
{
    'use strict';

    if (Application.originalModels[name])
    {
        if (!Application.extendedModels[name])
        {
            Application.pushToExtendedModels(name);
        }

        return Application.extendedModels[name];
    }
    else
    {
        throw nlapiCreateError('APP_ERR_UNKNOWN_MODEL', 'The model ' + name + ' is not defined');
    }

}