//AvailableFeature.js
// AvailableFeature.js
// -----------
// Handles the AvailableFeature for an item
Application.defineModel('GiftWrap', {

  get: function (itemId) {
    'use strict';

    var store_item = Application.getModel('StoreItem'),
        filters = [new nlobjSearchFilter("custrecord_giftwrap_parent", null, "is", itemId),
                     new nlobjSearchFilter("isinactive", null, "is", 'F')],
        columns = [new nlobjSearchColumn("custrecord_giftwrap_related"),
                   new nlobjSearchColumn("baseprice","custrecord_giftwrap_related"),
                   new nlobjSearchColumn("storedisplayname","custrecord_giftwrap_related"),
                   new nlobjSearchColumn("type","custrecord_giftwrap_related")],
                  giftwraps = [],
                  items = new nlapiSearchRecord("customrecord_matrix_giftwrap", null, filters, columns);

    store_item.preloadedItems = [];

    if (items && items.length > 0)	{
      for (var i = 0; i < items.length; i++)	{
        var itemPrice = items[i].getValue("baseprice","custrecord_giftwrap_related"),
            itemNum = items[i].getValue("custrecord_giftwrap_related"),
            itemName = items[i].getValue("storedisplayname","custrecord_giftwrap_related"),
            type = items[i].getValue("type", "custrecord_giftwrap_related"),
            giftwrap = {
              price: itemPrice,
              id: itemNum,
              name: itemName
            };

        store_item.preloadItems([
        {
          id: itemNum.toString(),
          type: type
        }]);

        if (store_item.get(itemNum.toString(), type)) {
          giftwrap.item = store_item.get(itemNum.toString(), type);
        }

        giftwraps.push(giftwrap);
      }
    }

    return giftwraps;
  }

});
