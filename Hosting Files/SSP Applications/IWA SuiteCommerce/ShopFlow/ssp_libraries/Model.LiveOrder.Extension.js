Application.on('before:LiveOrder.addLine', function(Model,parameters){
    'use strict';
    //console.time('before:LiveOrder.addLine');
    //nlapiLogExecution('ERROR', 'parameters', JSON.stringify(parameters));
    if (parameters.options && parameters.options.custcol_gift_wrap) {
        var itemId = parameters.options.custcol_gift_wrap;
        var quantity = parameters.quantity;
        var message = parameters.options.custcol_gift_wrap_message;
        var packageId = Model.getPackageId();
        var result = Model.addLine({item: {internalid: itemId}, 
                                    quantity: quantity, 
                                    options: {custcol_gift_wrap_message: message,
                                              custcol_is_gift_wrap: 'T',
                                              custcol_gift_wrap_package: packageId.toString()}});

        parameters.options.custcol_gift_wrap = result;
    }
    //nlapiLogExecution('ERROR', 'parameters2', JSON.stringify(parameters));
    //console.timeEnd('before:LiveOrder.addLine');
});

Application.on('before:LiveOrder.addLines', function(Model,allparameters){
    'use strict';
    //console.time('before:LiveOrder.addLines');
    for (var i = 0; i < allparameters.length; i++) {
        var parameters = allparameters[i];
        if (parameters.options && parameters.options.custcol_gift_wrap) {
            //nlapiLogExecution('ERROR', '1');
            var itemId = parameters.options.custcol_gift_wrap;
            var quantity = parameters.quantity;
            var message = parameters.options.custcol_gift_wrap_message;
            var packageId = Model.getPackageId();
            //nlapiLogExecution('ERROR', 'before add line');
            var result = Model.addLine({item: {internalid: itemId},
                quantity: quantity,
                options: {custcol_gift_wrap_message: message,
                    custcol_is_gift_wrap: 'T',
                    custcol_gift_wrap_package: packageId.toString()}});
            //nlapiLogExecution('ERROR', 'after add line');
            parameters.options.custcol_gift_wrap = result;
        }
    }

    //console.timeEnd('before:LiveOrder.addLines');
});


Application.on('before:LiveOrder.removeLine', function(Model,parameters){
    'use strict';    
    //console.time('before:LiveOrder.removeLine');
    Model.removeGiftWrapOrError(parameters);
    if (Model.isGiftWrap(parameters)) {
        throw nlapiCreateError('APP_ERR_GIFT_GRAP_REMOVE', 'This item can not be removed.');
    }
    else {
        Model.removeGiftWrap(parameters);    
    }
    //console.timeEnd('before:LiveOrder.removeLine');
});

Application.on('before:LiveOrder.updateLine', function(Model,parameters, data){
    'use strict';    
    //console.time('before:LiveOrder.updateLine');
    //nlapiLogExecution('ERROR', 'parameters', JSON.stringify(parameters));
    //nlapiLogExecution('ERROR', 'data', JSON.stringify(data));
    if (data.options && data.options.custcol_is_gift_wrap === 'T') {
        throw nlapiCreateError('APP_ERR_GIFT_GRAP_EDIT', 'This item can not be edited.');
    }
    else {
        if (data.options && data.options.custcol_gift_wrap) {
            //nlapiLogExecution('ERROR', '1');
            var giftObj = Model.getMessage(parameters);
            //nlapiLogExecution('ERROR', 'giftObj', JSON.stringify(giftObj));
            if (giftObj && data && data.options && data.options.custcol_gift_wrap_message) {
                data.options.custcol_gift_wrap = giftObj.id;
            }
            else if (giftObj){            
                data.options.custcol_gift_wrap_message = giftObj.message;
                data.options.custcol_gift_wrap = giftObj.id;
                
            }      
        }
          
    }
    //nlapiLogExecution('ERROR', 'data2', JSON.stringify(data));
    //console.timeEnd('before:LiveOrder.updateLine');
});

Application.extendModel('LiveOrder', {
    removeGiftWrap: function(itemid) {
        //console.time('removeGiftWrap');
        //console.time('order.getFieldValues removeGiftWrap');
        //var order_fields = order.getFieldValues();
        //console.timeEnd('order.getFieldValues removeGiftWrap');
        if (order.items && order.items.length) {
            var item = _.findWhere(order.items, {orderitemid: itemid});
            
            if (item) {                
                var giftWrap = _.findWhere(item.options, {id: 'CUSTCOL_GIFT_WRAP'});

                if (giftWrap && giftWrap.value) {              
                    order.removeItem(giftWrap.value);
                }
            }
        }
        //console.timeEnd('removeGiftWrap');
    },

    removeGiftWrapOrError: function(itemid) {
        //console.time('removeGiftWrapOrError');

        //console.timeEnd('order.getFieldValues isGiftWrap');
        if (order.items && order.items.length) {
            var item = _.findWhere(order.items, {orderitemid: itemid});
            if (item && item.options) {
                var isGiftWrap = _.findWhere(item.options, {id: 'CUSTCOL_IS_GIFT_WRAP'});
                if (isGiftWrap && isGiftWrap.value) {
                    throw nlapiCreateError('APP_ERR_GIFT_GRAP_REMOVE', 'This item can not be removed.');
                }
                var giftWrap = _.findWhere(item.options, {id: 'CUSTCOL_GIFT_WRAP'});

                if (giftWrap && giftWrap.value) {              
                    order.removeItem(giftWrap.value);
                }

            }
        }

        //console.timeEnd('removeGiftWrapOrError');
    },

    isGiftWrap: function(itemid) {
        //console.time('isGiftWrap');
        //console.time('order.getFieldValues isGiftWrap');
        //var order_fields = order.getFieldValues();
        //console.timeEnd('order.getFieldValues isGiftWrap');
        if (order.items && order.items.length) {
            var item = _.findWhere(order.items, {orderitemid: itemid});
            if (item && item.options) {
                var isGiftWrap = _.findWhere(item.options, {id: 'CUSTCOL_IS_GIFT_WRAP'});
                return isGiftWrap && isGiftWrap.value;    
            }
            
        }
        //console.timeEnd('isGiftWrap');
        return false;

    },

    getMessage: function(itemid) {
        //console.time('getMessage');
        //console.time('order.getFieldValues getMessage');
        //var order_fields = order.getFieldValues();
        //console.timeEnd('order.getFieldValues getMessage');
        if (order.items && order.items.length) {
            var item = _.findWhere(order.items, {orderitemid: itemid});
            //nlapiLogExecution('ERROR', 'item', JSON.stringify(item));
            if (item) {
                
                var giftWrap = _.findWhere(item.options, {id: 'CUSTCOL_GIFT_WRAP'});
                //nlapiLogExecution('ERROR', 'giftWrap', JSON.stringify(giftWrap));
                if (giftWrap && giftWrap.value) {
                    var giftObj = _.findWhere(order.items, {orderitemid: giftWrap.value});
                    //nlapiLogExecution('ERROR', 'giftObj', JSON.stringify(giftObj));
                    if (giftObj) {
                        var giftId = giftObj.internalid;
                        //nlapiLogExecution('ERROR', 'giftId', JSON.stringify(giftId));
                        //nlapiLogExecution('ERROR', 'giftObj.options', JSON.stringify(giftObj.options));
                        if (giftObj.options) {
                            var messageOption = _.findWhere(giftObj.options, {id: 'CUSTCOL_GIFT_WRAP_MESSAGE'});
                            if (messageOption) {
                                return {message: messageOption.value, id: giftId};
                            }
                        }    
                    }                    
                }
            }
        }
        //console.timeEnd('getMessage');
    },

    getItemGiftWrap: function(giftId) {
       // console.time('getItemGiftWrap');
        //console.time('order.getFieldValues getItemGiftWrap');
        //var order_fields = order.getFieldValues();
        //console.timeEnd('order.getFieldValues getItemGiftWrap');
        var giftItem;
        if (order.items && order.items.length) {
            
            _.each(order.items, function(item) {
                if (item.options) {
                    var gift = _.findWhere(item.options, {id: 'CUSTCOL_GIFT_WRAP'});
                    if (gift && (gift.value == giftId)){
                        giftItem = item;
                    }
                }
            });
        }
        //console.timeEnd('getItemGiftWrap');
        return giftItem;
    },

    updateLine: function (line_id, line_data)
    {
        'use strict';
        var itemObj = _.findWhere(order.items, {orderitemid: line_id});
        if (!itemObj) {
            return;
        }
        var lines_sort = this.getLinesSort()
            ,	current_position = _.indexOf(lines_sort, line_id)
            ,	original_line_object = order.getItem(line_id);
        //nlapiLogExecution('ERROR', 'updateLine', 1);
        this.removeLine(line_id);
        //nlapiLogExecution('ERROR', 'updateLine', 2);
        if (!_.isNumber(line_data.quantity) || line_data.quantity > 0)
        {
            var new_line_id;
            try
            {
                //nlapiLogExecution('ERROR', 'updateLine', JSON.stringify(line_data));
                new_line_id = this.addLine(line_data);
                //nlapiLogExecution('ERROR', 'updateLine', 4);
            }
            catch (e)
            {
                // we try to roll back the item to the original state
                var roll_back_item = {
                    item: { internalid: parseInt(original_line_object.internalid, 10) }
                    ,	quantity: parseInt(original_line_object.quantity, 10)
                };

                if (original_line_object.options && original_line_object.options.length)
                {
                    roll_back_item.options = {};
                    _.each(original_line_object.options, function (option)
                    {
                        roll_back_item.options[option.id.toLowerCase()] = option.value;
                    });
                }

                new_line_id = this.addLine(roll_back_item);

                e.errorDetails = {
                    status: 'LINE_ROLLBACK'
                    ,	oldLineId: line_id
                    ,	newLineId: new_line_id
                };

                throw e;
            }

            lines_sort = _.without(lines_sort, line_id, new_line_id);
            lines_sort.splice(current_position, 0, new_line_id);

            this.setLinesSort(lines_sort);
        }
    },

    getPackageId: function() {
        //console.time('getPackageId');
        //console.time('order.getFieldValues getPackageId');
        //var order_fields = order.getFieldValues();
       // console.timeEnd('order.getFieldValues getPackageId');
        var packageId = 0;

        _.each(order.items, function(item) {
            var isGiftWrap = _.findWhere(item.options, {id:'CUSTCOL_IS_GIFT_WRAP'});
            if (isGiftWrap && isGiftWrap.value === 'T') {
                var packageOption = _.findWhere(item.options, {id:'CUSTCOL_GIFT_WRAP_PACKAGE'});
                if (packageOption && parseInt(packageOption.value) > packageId) {
                    packageId = parseInt(packageOption.value);
                }
            }
        });
        //console.timeEnd('getPackageId');
        return packageId+1;

    },

    update: function (data)
    {
        'use strict';

        var current_order = this.get()
          ,	is_secure = request.getURL().indexOf('https') === 0
          ,	is_logged_in = session.isLoggedIn();

        // Promo code
        if (data.promocode && (!current_order.promocode || data.promocode.code !== current_order.promocode.code))
        {
            try
            {
                order.applyPromotionCode(data.promocode.code);
            }
            catch (e)
            {
                order.removePromotionCode(data.promocode.code);
                current_order.promocode && order.removePromotionCode(current_order.promocode.code);
                throw e;
            }
        }
        else if (!data.promocode && current_order.promocode)
        {
            order.removePromotionCode(current_order.promocode.code);
        }

        // Billing Address
        if (data.billaddress !== current_order.billaddress)
        {
            if (data.billaddress)
            {
                if (data.billaddress && !~data.billaddress.indexOf('null'))
                {
                    // Heads Up!: This "new String" is to fix a nasty bug
                    order.setBillingAddress(new String(data.billaddress).toString());
                }
            }
            else if (is_secure)
            {
                // remove the address
                try
                {
                    order.setBillingAddress('0');
                } catch(e) { }
            }


        }

        // Ship Address
        if (data.shipaddress !== current_order.shipaddress)
        {
            if (data.shipaddress)
            {
                if (is_secure && !~data.shipaddress.indexOf('null'))
                {
                    // Heads Up!: This "new String" is to fix a nasty bug
                    order.setShippingAddress(new String(data.shipaddress).toString());
                }
                else
                {
                    var address = _.find(data.addresses, function (address)
                    {
                        return address.internalid === data.shipaddress;
                    });

                    address && order.estimateShippingCost(address);
                }
            }
            else if (is_secure)
            {
                // remove the address
                try
                {
                    order.setShippingAddress('0');
                } catch(e) { }
            }
            else
            {
                order.estimateShippingCost({
                    zip: null
                    ,	country: null
                });
            }
        }

        //Because of an api issue regarding Gift Certificates, we are going to handle them separately
        var gift_certificate_methods = _.where(data.paymentmethods, {type: 'giftcertificate'})
            ,	non_certificate_methods = _.difference(data.paymentmethods, gift_certificate_methods);

        // Payment Methods non gift certificate
        if (is_secure && non_certificate_methods && non_certificate_methods.length)
        {

            _.sortBy(non_certificate_methods, 'primary').forEach(function (paymentmethod)
            {
                if (paymentmethod.type === 'creditcard' && paymentmethod.creditcard)
                {

                    var credit_card = paymentmethod.creditcard
                        ,	require_cc_security_code = session.getSiteSettings(['checkout']).checkout.requireccsecuritycode === 'T'
                        ,	cc_obj = credit_card && {
                            internalid: credit_card.internalid
                            ,	ccnumber: credit_card.ccnumber
                            ,	ccname: credit_card.ccname
                            ,	ccexpiredate: credit_card.ccexpiredate
                            ,	expmonth: credit_card.expmonth
                            ,	expyear:  credit_card.expyear
                            ,	paymentmethod: {
                                internalid: credit_card.paymentmethod.internalid
                                ,	name: credit_card.paymentmethod.name
                                ,	creditcard: credit_card.paymentmethod.creditcard ? 'T' : 'F'
                                ,	ispaypal:  credit_card.paymentmethod.ispaypal ? 'T' : 'F'
                            }
                        };

                    if (credit_card.ccsecuritycode)
                    {
                        cc_obj.ccsecuritycode = credit_card.ccsecuritycode;
                    }

                    if (!require_cc_security_code || require_cc_security_code && credit_card.ccsecuritycode)
                    {
                        // the user's default credit card may be expired so we detect this using try&catch and if it is we remove the payment methods.
                        try
                        {
                            order.setPayment({
                                paymentterms: 'CreditCard'
                                ,	creditcard: cc_obj
                            });

                            context.setSessionObject('paypal_complete', 'F');
                        }
                        catch(e)
                        {
                            if (e && e.code && e.code === 'ERR_WS_INVALID_PAYMENT' && is_logged_in)
                            {
                                order.removePayment();
                            }
                            throw e;
                        }
                    }
                    // if the the given credit card don't have a security code and it is required we just remove it from the order
                    else if(require_cc_security_code && !credit_card.ccsecuritycode)
                      {
                        order.removePayment();
                      }
                }
                else if (paymentmethod.type === 'invoice')
                {
                    order.setPayment({ paymentterms: 'Invoice' });
                    paymentmethod.purchasenumber && order.setPurchaseNumber(paymentmethod.purchasenumber);

                    context.setSessionObject('paypal_complete', 'F');
                }
                else if (paymentmethod.type === 'paypal')
                {
                    var paypal = _.findWhere(session.getPaymentMethods(), {ispaypal: 'T'});
                    order.setPayment({paymentterms: '', paymentmethod: paypal.internalid});
                }
            });

        }else if (is_secure && is_logged_in)
        {
            order.removePayment();
        }

        // Payment Methods gift certificate
        if (is_secure && gift_certificate_methods && gift_certificate_methods.length)
        {
            //Remove all gift certificates so we can re-enter them in the appropriate order
            order.removeAllGiftCertificates();
            _.forEach(gift_certificate_methods, function (certificate)
            {
                order.applyGiftCertificate(certificate.giftcertificate.code);
            });
        }

        // Shipping Method
        if (data.shipmethod !== current_order.shipmethod)
        {
            var shipmethod = _.where(current_order.shipmethods, {internalid: data.shipmethod})[0];
            shipmethod && order.setShippingMethod({
                shipmethod: shipmethod.internalid
                ,	shipcarrier: shipmethod.shipcarrier
            });
        }

        // Terms and conditions
        var require_terms_and_conditions = session.getSiteSettings(['checkout']).checkout.requiretermsandconditions;
        if (require_terms_and_conditions.toString() === 'T' && is_secure && !_.isUndefined(data.agreetermcondition))
        {
            order.setTermsAndConditions(data.agreetermcondition);
        }

        // Transaction Body Field
        if (is_secure && !_.isEmpty(data.options))
        {
            order.setCustomFieldValues(data.options);
        }

    }
 });
