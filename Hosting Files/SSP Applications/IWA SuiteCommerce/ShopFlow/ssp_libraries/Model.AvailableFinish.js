//AvailableFeature.js
// AvailableFeature.js
// -----------
// Handles the AvailableFeature for an item
Application.defineModel('AvailableFinish', {
		
	get: function (itemid) {
		'use strict';

		var store_item = Application.getModel('StoreItem'),
        filters = [new nlobjSearchFilter('custrecord_availablefinishitemfinishof', null, 'is', itemid),
                    new nlobjSearchFilter('isinactive', null, 'is', 'F')],
        columns = [new nlobjSearchColumn('custrecord_availablefinishitem'),
                   new nlobjSearchColumn('custrecord_availablefinishitemname'),
                   new nlobjSearchColumn('custrecord_availablefinishitemprice'),
                   new nlobjSearchColumn('type', 'custrecord_availablefinishitem')],
                   results = nlapiSearchRecord('customrecord_availablefinish', null, filters, columns),
                   availableFinishes = [];

		store_item.preloadedItems = [];

		_.each(results, function (result, index) {
			var id = result.getValue(columns[0]),
			    type = result.getValue(columns[3]);

			store_item.preloadItems([
      { id: id.toString(),
				type: type
      }]);

			if (store_item.get(id.toString(), type)) {
				var finish = store_item.get(id.toString(), type);
				availableFinishes.push(finish);	
			}
			
		});

		return availableFinishes;
	}

});
