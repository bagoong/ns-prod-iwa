//LiveOrder.js
// -------
// We show only the shipping methods the items in the order can have.
// If we have two or more items with one or more shipping methods in common we enable the user to choose between those common shipping methods.
// If we have two or more items that don't have any shipping methods in common we let the user choose between the union of shipping methods of the items.
// If the items in the order don't have shipping methods associated we assume there is no restriction and allow the user to chosse between all shipping methods available in the site,
Application.on('after:LiveOrder.get', function(Model, response){
    'use strict';
    try {
    	console.time('after:LiveOrder.get-shipmethods');
	    var websiteshippingmethods = response.shipmethods,
	        lines = response.lines;

	    var shippingmethods = [],
			commonshippingmethods = [],
			availableshippingmethods = _.map(websiteshippingmethods, function(shipmethod) {
				return shipmethod.name;
			});

		var index = 0,
			is_secure = request.getURL().indexOf('https') === 0,
        	is_logged_in = session.isLoggedIn();

		_.each(lines, function(line) {
			if (line.item) {
				var itemShipMethods = line.item.custitemship_methods;
				//nlapiLogExecution('ERROR', 'itemShipMethods', JSON.stringify(itemShipMethods));
				if (itemShipMethods) {
					var ships = itemShipMethods.split(',');
					if (ships) {
						for (var i = 0; i < ships.length; i++) {
							var shipmethod = ships[i].trim();
							ships[i] = shipmethod;
							if ((shippingmethods.indexOf(shipmethod) == -1) && (availableshippingmethods.indexOf(shipmethod) != -1)) {
								shippingmethods.push(shipmethod);
							}
						}	

						if (index == 0) {
							commonshippingmethods = shippingmethods.slice(0);
						}
						else {
							
							var lastCommons = commonshippingmethods.slice(0);
							commonshippingmethods = [];
							for (var j = 0; j < lastCommons.length; j++) {
								var shipmethod = lastCommons[j].trim();

								if ((ships.indexOf(shipmethod) != -1) && (availableshippingmethods.indexOf(shipmethod) != -1)) {
									commonshippingmethods.push(shipmethod);
								}
							} 
						}
					}	
				}
				else {
					commonshippingmethods = [];
				}
				index++;
				
			}
		});

		var orderShipMethods = [];
		_.each(websiteshippingmethods, function(shipmethod)
		{
			
			if (_.contains(commonshippingmethods, shipmethod.name) || 
				((!commonshippingmethods || !commonshippingmethods.length) && 
				(!shippingmethods || !shippingmethods.length || 
				_.contains(shippingmethods, shipmethod.name)))) {
				
				orderShipMethods.push(shipmethod);	
			}
			
		});
	    
	    if(orderShipMethods.length > 0){
	    	response.shipmethods = orderShipMethods;
	        if(!response.shipmethod || _.findWhere(orderShipMethods,{internalid: response.shipmethod})){
	        	if (orderShipMethods.length == 1) {
	        		if (response.shipaddress && response.shipaddress !== '-------null') {
	        			var shipmethod = orderShipMethods[0];

		        		if (shipmethod.internalid != parseInt(response.shipmethod)) {
							order.setShippingMethod({shipmethod: shipmethod.internalid,
		                						    shipcarrier: shipmethod.shipcarrier});
							_.extend(response,Model.get());	
						}
					}
	        	}
	            return;
	        }
	        else if (response.shipmethod && !_.findWhere(orderShipMethods,{internalid: response.shipmethod})) {
	        	if (response.shipaddress && response.shipaddress !== '-------null') {
	        		var shipmethod = orderShipMethods[0];
					
					if (shipmethod.internalid != parseInt(response.shipmethod)) {
						order.setShippingMethod({shipmethod: shipmethod.internalid,
	                						    shipcarrier: shipmethod.shipcarrier});
						_.extend(response,Model.get());	
					}
					
				}
	        }
	    } else {
	    	response.shipmethods =  websiteshippingmethods;
	    }

	}
	catch(e) {
		nlapiLogExecution('ERROR', 'e', e);
	}
	console.timeEnd('after:LiveOrder.get-shipmethods');
});