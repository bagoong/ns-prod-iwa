define('Profile.UpdatePassword.Model.Extension', ['Profile.UpdatePassword.Model'], function (Model) {

  'use strict';

  _.extend(Model.prototype, {

    validation: _.extend(Model.prototype.validation, {
      password: [
        { required: true, msg: _('New password is required').translate() },
        { fn: _.validatePassword }
      ]
    })

  });

  return Model;

});
