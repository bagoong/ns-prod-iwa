define('Address.Model.Extension', ['Address.Model'], function (Model) {

  'use strict';

  _.extend(Model.prototype, {

    validation: _.extend(Model.prototype.validation, {
			fullname: [
        { required: true, msg: _('Full Name is required').translate() },
        { fn: _.validateFullName }
      ],

			addr1: [
        { required: true, msg: _('Address is required').translate() },
        { fn: _.validateAddress }
      ]
    })

  });

  return Model;

});
