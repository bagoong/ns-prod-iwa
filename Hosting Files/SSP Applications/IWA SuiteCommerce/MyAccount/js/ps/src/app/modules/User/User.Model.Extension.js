define('User.Model.Extension', ['User.Model', 'Address.Collection', 'CreditCard.Collection'], function (Model, AddressCollection, CreditCardsCollection) {

  'use strict';

  _.extend(Model.prototype, {

    validation: _.extend(Model.prototype.validation, {
      firstname: [
        { required: true, msg: _('First Name is required').translate() },
        { fn: _.validateFirstName }
      ],

      lastname: [
        { required: true, msg: _('Last Name is required').translate() },
        { fn: _.validateLastName }
      ],

      email: [
        { required: true, pattern: 'email', msg: _('Valid Email is required').translate() },
        { fn: _.validateEmail }
      ]
    }),

    initialize: function (attributes) {
      this.on('change:addresses', function (model, addresses) {
        model.set('addresses', new AddressCollection(addresses), { silent: true });
        this.get('addresses').on('change:defaultshipping change:defaultbilling add destroy reset', this.checkDefaultsAddresses, this);
      });

      this.on('change:creditcards', function (model, creditcards) {
        model.set('creditcards', new CreditCardsCollection(creditcards), { silent: true });
        this.get('creditcards').on('change:ccdefault add destroy reset', this.checkDefaultsCreditCard, this);
      });

      this.on('change:lastname', function (model, lastname) {
        model.validation.lastname = [
          { required: true, msg: _('Last Name is required').translate() },
          { fn: _.validateLastName }
        ];
      });			

      this.on('change:phone', function (model, lastname) {
        model.validation.phone = { required: true, fn: _.validatePhone };
      });

      this.on('change:balance', function (model) {
        var balance_available = model.get('creditlimit') - model.get('balance');
        model.set('balance_available', balance_available);
        model.set('balance_available_formatted', _.formatCurrency(balance_available));
      });

      this.set('addresses', attributes && attributes.addresses || new AddressCollection());
      this.set('creditcards', attributes && attributes.creditcards || new CreditCardsCollection());
    }

  });

  return Model;

});
