// ItemDetails.Model.js
// --------------------
// Represents 1 single product of the web store

define('ItemDetails.Model.Extension', ['ItemDetails.Model'], function (Model) {

  'use strict';
	var getOrig = Model.prototype.get;
	_.extend(Model.prototype, {

		getItemOptionLabel: function (option, value)
		{
			var optionsDetails = this.get('_optionsDetails');
			var optionField = _.findWhere(optionsDetails.fields, {internalid: option});
			var optionValue = _.findWhere(optionField.values, {internalid: value});
			return optionValue.label;
		},

		setOption: function(option_name, value, dont_validate)
		{
			// Setting it to null means you dont wan a value for it
			if (option_name === 'quantity')
			{
				this.set('quantity', parseInt(value, 10) || 1);
			}
			else if (_.isNull(value))
			{
				delete this.itemOptions[option_name];
			}
			else
			{
				// Sometimes the name comes in all uppercase
				var option = this.getPosibleOptionByCartOptionId(option_name) || this.getPosibleOptionByCartOptionId(option_name.toLowerCase());

				if (option) {
					// You can pass in the internalid on the instead of the full item 
					if (option.values)
					{
						value = _.isObject(value) ? value : _.where(option.values, {internalid: value.toString()})[0];
					}
					else if (!_.isObject(value))
					{
						value = {
							internalid: value
						,	label: value
						};
					}	
				
				
				
					// if it's a matrix option this will make sure it's compatible to what its already set!
					if (!dont_validate && option.isMatrixDimension && !_.contains(this.getValidOptionsFor(option.itemOptionId), value.label))
					{
						throw new RangeError('The combination you selected is invalid');
					}

					this.itemOptions[option.cartOptionId] = value;
				}
			}

			return value;
		}
		
	});

    return Model;

});
