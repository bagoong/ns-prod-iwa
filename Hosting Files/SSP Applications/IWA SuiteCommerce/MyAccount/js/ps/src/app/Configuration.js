(function (application) {

    'use strict';

    application.on('beforeStartApp', function() {

        var configuration = application.Configuration;

        application.addModule('Address.Model.Extension');
        application.addModule('ItemDetails.Model.Extension');
        application.addModule('Profile.UpdatePassword.Model.Extension');
        application.addModule('User.Model.Extension');
        application.addModule('GoogleUniversalAnalytics');
        application.addModule('OrderHistory.Views');

        _.extend(configuration, {

            // Analytics Settings
            // You need to set up both popertyID and domainName to make the default trackers work
            tracking: {
                trackPageview: true
                // [Google Universal Analytics](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
                , googleUniversalAnalytics: {
                    propertyID: 'UA-9999920-1'
                    , domainName: 'webstorecheckout.com'
                }
            }

        });

    });

}(SC.Application('MyAccount')));
