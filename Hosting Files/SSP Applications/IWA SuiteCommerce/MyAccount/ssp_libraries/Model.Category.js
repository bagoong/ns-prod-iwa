Application.defineModel('Category', {

    // if the root_id is blank then we should get all the top level categories of the site
    get: function ()
    {
        'use strict';

        return this.fixCategories(session.getSiteCategoryContents({internalid:'-200'}, false), '');
    }

    
    ,	fixCategories: function (categories, url)
    {
        'use strict';

        var result = {}
            ,	self = this;

        _.each(categories, function (category)
        {

            if(category.internalid > 0){
                category.urlcomponent = category.urlcomponent || category.itemid;
                category.url = '';
                url = url || '';
                if (category.urlcomponent) {
                    category.url = url + '/' + category.urlcomponent;    
                }

                var category_limited =  _.omit(category, 'storedisplaythumbnailhtml','categorythumbnailhtml','canonicalurl','storeurl','welcomepagetitle','storedisplayimagehtml', 'storedetaileddescription2', 'storedetaileddescription');
                
                result[category_limited.urlcomponent] = category_limited;

                if (category_limited.categories && category_limited.categories.length)
                {
                    category_limited.categories = self.fixCategories(category_limited.categories, category_limited.url);
                }
            } else {
                result = self.fixCategories(category.categories);
            }
        });

        return result;
    }

});
