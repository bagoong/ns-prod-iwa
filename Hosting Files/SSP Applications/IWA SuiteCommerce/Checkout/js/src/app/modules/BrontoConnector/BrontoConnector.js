define('BrontoConnector', [], function () {
		
	'use strict';

	// Provide the Connector instance ID
	var INSTANCE_ID = "bc6e94abc2fab3e795915962b82db92876fe73f3dbcc5e3f678164623585045a";

	return {
		mountToApp: function (application) {
			if (SC.ENVIRONMENT.jsEnvironment === 'browser') {
				jQuery("body").append(jQuery('<script src="https://cdn.bronto.com/netsuite/configure.js" data-bronto-integrations="' + INSTANCE_ID + '"></script>'));
			}
		}
	};

});

function findApplication() {
	var isDefined = function(variable) {
		return typeof(variable) !== "undefined" && variable !== null;
	};
	var isValid = function(app) {
		return isDefined(app) && isDefined(app.Configuration) && isDefined(app.Configuration.modules);
	};
	var applications = [SC._applications.Shopping, SC._applications.Checkout, SC._applications.MyAccount];
	var application = null;
	for (var i = 0; i < applications.length; i++) {
		if (isValid(applications[i])) {
			application = applications[i];
			break;
		}
	}
	return application;
}

(function (Application) {
	"use strict";
	if (typeof(Application) !== "undefined" && Application != null) {
		Application.Configuration.modules.push('BrontoConnector');
	}
})(findApplication());