define('OrderWizard.Module.CustomTransactionFields', ['Wizard.Module'], function (WizardModule) {

  'use strict';

  var $ = jQuery;

  return WizardModule.extend({

    template: 'order_wizard_customtransactionfields_module',

    submit: function () {
      this.model.set('options', {
        custbody3: _.escape($('#custbody3').val())
      });

      return this.isValid();
    }

  });

});
