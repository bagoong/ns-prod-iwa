// ItemDetails.Model.js
// --------------------
// Represents 1 single product of the web store

define('ItemDetails.Model.Extension', ['ItemDetails.Model'], function (Model) {

  'use strict';

  var getOrig = Model.prototype.get;

  _.extend(Model.prototype, {

    getItemOptionLabel: function (option, value) {
      var optionsDetails = this.get('_optionsDetails'),
          optionField = _.findWhere(optionsDetails.fields, {internalid: option}),
          optionValue = _.findWhere(optionField.values, {internalid: value});

      return optionValue.label;
    }

  });

  return Model;

});
