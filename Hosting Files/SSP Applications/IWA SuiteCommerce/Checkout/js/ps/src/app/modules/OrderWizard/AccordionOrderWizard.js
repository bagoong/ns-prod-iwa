define('AccordionOrderWizard', ['OrderWizard','AccordionOrderWizard.View', 'AccordionOrderWizard.Router'], function(OrderWizard,View,Router){
    return {
        View: View,
        Router: Router
    };
});
define('AccordionOrderWizard.View', ['OrderWizard.View'],function(View){
    _.extend(View.prototype, {
        template: 'order_wizard_accordion_wizard'
    });
	
	View.prototype.initialize = _.wrap(View.prototype.initialize, function (fn, view)
	{
		jQuery( window ).scroll(function() {
			var summary = jQuery('#order-summary .checkout-cart-summary');
			if( jQuery( window ).scrollTop() > 174 )
			{
				jQuery(summary).addClass('fixed');
			}else
			{
				jQuery(summary).removeClass('fixed');
			}
		});
		
		return fn.apply(this, _.toArray(arguments).slice(1));
	});
	
});
define('AccordionOrderWizard.Router', ['OrderWizard.Router'], function(Router){
    _.extend(Router.prototype, {
        runStep: function()
        {
            var url = Backbone.history.fragment
                ,	self = this;

            // We allow urls to have options but they are still identified by the original string,
            // so we need to thake them out if present
            url = url.split('?')[0];


            if (this.steps[url])
            {
                // We keep a reference to the current step url here
                this.currentStep = url;

                // Iterates all the steps and calls the status methods
                var method_to_call = 'past'
                    ,	current_group;
                _.each(this.stepsOrder, function(step)
                {
                    if (step === url)
                    {
                        self.steps[step].present();
                        self.steps[step].state = 'present';
                        self.steps[step].stepGroup.state = 'present';
                        self.steps[step].tellModules('present');
                        method_to_call = 'future';
                        current_group = self.steps[step].stepGroup;
                    }
                    else
                    {
                        self.steps[step].tellModules(method_to_call);
                        self.steps[step][method_to_call]();
                        self.steps[step].state = method_to_call;

                        // if the step is contained in the current_group we don't change the group state
                        if (self.steps[step].stepGroup !== current_group)
                        {
                            self.steps[step].stepGroup.state = method_to_call;
                        }
                    }
                });

                // Creates an instance of the frame view and pass the current step
                var view = new this.view({
                    model: this.model
                    ,	wizard: this
                    ,	currentStep: this.steps[url]
                    ,	application: this.application
                });

                /* PS Customization for not scrolling */
                view.showContent(true);
            }
        }
    })
});
