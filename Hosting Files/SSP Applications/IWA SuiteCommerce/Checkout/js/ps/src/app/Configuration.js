(function (application) {

    'use strict';

    application.on('beforeStartApp', function() {

        var configuration = application.Configuration;

        application.addModule('Content');
        application.addModule('Account.Register.Model.Extension');
        application.addModule('Address.Model.Extension');
        application.addModule('ItemDetails.Model.Extension');
        application.addModule('AccordionOrderWizard');
        application.addModule('OrderWizard.Module.CustomTransactionFields');
        application.addModule('OrderWizard.Module.RegisterEmail.Extension');
        application.addModule('OrderWizard.Step.Extension');
        application.addModule('Wizard.View.Extension');
        application.addModule('GoogleUniversalAnalytics');
      application.addModule('Categories');

        _.extend(configuration, {

            creditCardIcons: {
                'Visa': 'img/visa.png',
                'Discover': 'img/discover.png',
                'MasterCard': 'img/master.png',
                'American Express': 'img/american.png'
            },

            // Analytics Settings
            // You need to set up both popertyID and domainName to make the default trackers work
            tracking: {
                // [Google Universal Analytics](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
                googleUniversalAnalytics: {
                    propertyID: 'UA-9999920-1'
                    , domainName: 'webstorecheckout.com'
                }
            },

            checkoutSteps: [
                {
                    name: _('Shipping').translate(),
                    steps: [
                        {
                            name: _('Shipping Address').translate(),
                            hideSecondContinueButtonOnPhone: true,
                            hideBackButton: true,
                            hideBreadcrumb: true,
                            getName: function () {
                                return _('Shipping Address').translate();
                            },
                            url: 'shipping/address',
                            modules: [
                                'OrderWizard.Module.Address.Shipping',
                                'OrderWizard.Module.Shipmethod'
                            ]
                        }
                    ]
                },
                {
                    name: _('Gift Certificate').translate(),
                    steps: [
                        {
                            name: _('Gift Certificate? If not, click the Continue button below.').translate(),
                            url: 'gift',
                            hideBreadcrumb: true,
                            modules: [
                                'OrderWizard.Module.PaymentMethod.GiftCertificates'
                            ]
                        }]
                },
                {
                    name: _('Payment').translate(),
                    steps: [
                        {
                            name: _('Payment Method').translate(),
                            url: 'billing',
                            hideBreadcrumb: true,
                            modules: [
                                ['OrderWizard.Module.Address.Billing', {enable_same_as: true, title: _('Billing Address').translate()}],
                                ['OrderWizard.Module.PaymentMethod.Selector', {
                                    modules: [
                                        {
                                            classModule: 'OrderWizard.Module.PaymentMethod.Creditcard',
                                            name: _('Credit / Debit Card').translate(),
                                            type: 'creditcard',
                                            options: {}
                                        },
                                        {
                                            classModule: 'OrderWizard.Module.PaymentMethod.Invoice',
                                            name: _('Invoice').translate(),
                                            type: 'invoice',
                                            options: {}
                                        },
                                        {
                                            classModule: 'OrderWizard.Module.PaymentMethod.PayPal',
                                            name: _('PayPal').translate(),
                                            type: 'paypal',
                                            options: {backFromPaypalBehavior: 'stay'}
                                        }
                                    ]}],
                                'OrderWizard.Module.RegisterEmail'
                            ]
                        }]
                },
                {
                    name: _('Review & Place order').translate(),
                    steps: [
                        {
                            name: _('Review Your Order').translate(),
                            url: 'review',
                            continueButtonLabel: _('Place Order').translate(),
                            hideBackButton: true,
                            hideBreadcrumb: true,
                            modules: [
                                ['OrderWizard.Module.CustomTransactionFields', { title: _('Other Shipping Info').translate() }],
                                ['OrderWizard.Module.ShowPayments', { edit_url_billing: '/billing', edit_url_address: '/billing' }],
                                ['OrderWizard.Module.ShowShipments', { edit_url: '/shipping/address', show_edit_button: true }],
                                'OrderWizard.Module.TermsAndConditions'
                            ],
                            save: function () {
                                return this.wizard.model.submit();
                            }
                        },
                        {
                            url: 'confirmation',
                            headerMacro: 'header',
                            hideSummaryItems: false,
                            hideContinueButton: true,
                            hideBreadcrumb: true,
                            hideBackButton: true,
                            modules: [
                                'OrderWizard.Module.Confirmation',
                                'OrderWizard.Module.RegisterGuest',
                                'OrderWizard.Module.ShowPayments',
                                'OrderWizard.Module.ShowShipments'
                            ],
                            present: function () {
                                this.wizard.application.trackTransaction(this.wizard.model);
                            }
                        }]
                }]

        });

    });

}(SC.Application('Checkout')));
