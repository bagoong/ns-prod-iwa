define('OrderWizard.Step.Extension', ['OrderWizard.Step'], function (Module) {

  'use strict';

  _.extend(Module.prototype, {

    headerMacro: 'header',

    footerMacro: 'footer'

  });

  return Module;

});
