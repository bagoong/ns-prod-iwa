(function(application) {

    'use strict';

    /**
     * Needed in order to use the custom events of Default SCA.
     * Do not remove.
     */
    application.start = _.wrap(SC.ApplicationSkeleton.prototype.start, function(fn)
    {
        var wizard_modules = _(this.getConfig('checkoutSteps')).chain().pluck('steps').flatten().pluck('modules').flatten().value();

        wizard_modules = _.uniq(wizard_modules);

        this.Configuration.modules = _.union(this.getConfig('modules'), wizard_modules);

        fn.apply(this, _.toArray(arguments).slice(1));
    });

})(SC.Application('Checkout'));