define('Account.Register.Model.Extension', ['Account.Register.Model', 'Utils.Extension'], function (Model, Utils) {

  'use strict';

  _.extend(Model.prototype, {

    validation: _.extend(Model.prototype.validation, {
      firstname: [
        { required: true, msg: _('First Name is required').translate() },
        { fn: _.validateFirstName }
      ],

      lastname: [
        { required: true, msg: _('Last Name is required').translate() },
        { fn: _.validateLastName }
      ],

      email: [
        { required: true, pattern: 'email', msg: _('Valid Email is required').translate() },
        { fn: _.validateEmail }
      ],

      password: [
        { required: true, msg: _('Please enter a valid password').translate() },
        { fn: _.validatePassword }
      ],

      password2: [
        { required: true, msg: _('Confirm password is required').translate() },
        { equalTo: 'password', msg: _('New Password and Confirm Password do not match').translate() },
        { fn: _.validatePassword }
      ]
    })

  });

  return Model;

});
