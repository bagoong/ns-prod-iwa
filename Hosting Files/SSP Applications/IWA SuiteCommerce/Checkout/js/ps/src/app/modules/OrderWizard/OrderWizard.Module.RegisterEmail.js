define('OrderWizard.Module.RegisterEmail.Extension', ['OrderWizard.Module.RegisterEmail'], function (Module) {

  'use strict';

  _.extend(Module.prototype, {

    submit: function () {
      
      var billingAddress = this.step.moduleInstances[0].getSelectedAddress();
      if(billingAddress.id){
          var profile = this.profile,
          fake_promise = jQuery.Deferred(),
          self = this,
          fullName = billingAddress.get('fullname'),
          email = this.$('input[name=email]').val(),
          emailsubscribe = this.$('input[name=sign-up-newsletter]').is(':checked') ? 'T' : 'F',
          firstName=firstName = fullName.split(' ', 1)[0],
          lastName = fullName.substring(firstName.length + 1),
          companyName = billingAddress.get('company'),
          options = {
            email: email,
            confirm_email: email
          };

      // if the user is not guest or not change the current values we just resolve the promise
      if (profile.get('isGuest') !== 'T' ||
          (profile.get('email') === email
            && profile.get('firstname') === firstName
            && profile.get('lastname') === lastName
            && profile.get('companyname') === companyName
            && profile.get('emailsubscribe') === emailsubscribe)) {
        return this.isValid();
      }

      if (!_.isEmpty(firstName)) {
        options.firstname = firstName;
      }

      if (!_.isEmpty(lastName)) {
        options.lastname = lastName;
      }

      if (!_.isEmpty(fullName)) {
        options.name = fullName;
      }

      if (!_.isEmpty(companyName)) {
        options.companyname = companyName;
      }

      profile.set(options);

      this.isValid().then(function () {
        // TODO: do we need to subscribe the guest to a campaign???
        profile.set('emailsubscribe', emailsubscribe)
               .save()
               .then(function () {
                 self.render();
                 fake_promise.resolve();
               }, function (message)	{
                 fake_promise.reject(message);
               });
      }, function (message)	{
        fake_promise.reject(message);
      });

      return fake_promise;
    }
  }

  });

});
