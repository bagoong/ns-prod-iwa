define('Wizard.View.Extension', ['Wizard.View'], function (View) {

  'use strict';

  var ViewPrototype = View.prototype,
      $ = jQuery;

  _.extend(ViewPrototype, {

    render: _.wrap(ViewPrototype.render, function (fn) {
      fn.apply(this, _.toArray(arguments).slice(1));

      this.options.application.getLayout().once('afterAppendView', function (view) {
        var $group = $('#wizard-content').closest('.accordion-group'),
            top;

        if ($group.length) {
          top = $group.offset().top + 'px';
          $('html, body').animate({ scrollTop: top }, 500);
        }
      });
    })

  });

});
