<% registerMacro('siteSearch', function (name, view) { %>
<% var maxLength = view.application.getConfig('searchPrefs.maxLength') || 0; %>
<style>
    input.search-query::-webkit-input-placeholder {
        color:#cccccc ;
        letter-spacing: 1px;
        font-weight: 100;
    }

    input.search-query::-moz-placeholder {
        color:#cccccc ;
        letter-spacing: 1px;
        font-weight: 100;
    }

    input.search-query::-ms-placeholder {
        color:#cccccc ;
        letter-spacing: 1px;
        font-weight: 100;
    }

    input.search-query::placeholder {
        color:#cccccc ;
        letter-spacing: 1px;
        font-weight: 100;
    }
</style>

<form class="form-search site-search" method="GET" action="/search">
    <div class="input-append">
        <!--
            <button type="submit" class="btn" style="background-image: url('http://iwawine.sandbox.netsuitestaging.com/SSP%20Applications/IWA%20Reference/Reference%20ShopFlow%201.04.0/img/magnifying-glass-sm.png'); border:none; background-repeat: no-repeat; background-color: white;">
                <i class="icon-search" ></i>
            </button>
        -->
        <button type="submit" class="" style="background-image: url('http://iwawine.sandbox.netsuitestaging.com/SSP%20Applications/IWA%20Reference/Reference%20ShopFlow%201.04.0/img/magnifying-glass-sm.png');border:none;background-repeat: no-repeat;background-color: white;padding: 9px 12px;z-index: 100;position: relative;display: inline-block;">
            <i class="" ></i>
        </button>

        <input class="input-medium search-query" placeholder="<%- _('Search').translate() %>" type="search" name="<%- name %>" autocomplete="off"
        <%= maxLength > 0 ? 'maxlength="'+maxLength+'"' : '' %>	style="border: 1px solid #333333;border-radius: 20px;letter-spacing: 1px;padding-left: 34px;position: relative;display: inline-block;margin-left: -34px; font-family: Arial; font-size: 16px;" >
    </div>
</form>
<%})%>


