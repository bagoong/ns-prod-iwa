<div class="row">
	<div class="span5">

		<h2><%= _('Reset Password').translate() %></h2>

		<div>
			<form>
				<p><%= _('Enter a new password below for <b>$(0)</b>').translate(view.email) %></p>
				<fieldset>
					<div data-type="alert-placeholder"></div>
					
					<div class="control-group">
						<label class="control-label" for="password"><%= _('Password <small>(required)</small>').translate() %></label>
						<div class="controls">
							<input type="password" class="input-large" id="password" name="password" value="">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="confirm_password"><%= _('Confirm Password <small>(required)</small>').translate() %></label>
						<div class="controls">
							<input type="password" class="input-large" id="confirm_password" name="confirm_password" value="">
						</div>
					</div>

					<button type="submit" class="btn btn-wine-red btn-block"><%=_('Change password').translate()%></button>
					<br>
					<p><a href="/login-register" data-target=".register" class="btn btn-wine-gray"><%= _('Cancel & Return To Sign In > ').translate()%></a></p>
				</fieldset>
			</form>
			
		</div>

	</div>
</div>
