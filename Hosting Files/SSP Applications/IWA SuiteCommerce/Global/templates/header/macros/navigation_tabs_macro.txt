<% registerMacro('navigationTabs', function (item, isTop, level) { %>
<%
  if (!item.data) {
    item.data = {};
  }
  // we want all navbar links to preserve current url option 'display'

  if (item['href']) {
    item['href'] = item['href'] + '/';
  }
  if (item.data['hashtag']) {
    item.data['hashtag'] = item.data['hashtag'] + '/';
  }

  item.data['keep-options'] = 'display';
%>
<%
  var levelClass = _.isNumber(level) ? ' level-' + level : '',
      MAX_CATEGORY_ROWS = 5;
%>
<% if (item.categories && item.categories.length) { %>
	<% if (isTop) { %>
		<li class="dropdown <%= levelClass %>">
			<a <%= _.objectToAtrributes(item) %> class="<%= levelClass %>">
				<%= item.text %>
			</a>
			<div class="dropdown-menu <%= levelClass %>">
        <ul class="category-row <%= levelClass %>">
          <% _.each(item.categories, function (sub, index) { %>
            <% if (index % MAX_CATEGORY_ROWS === 0 && index > 0) { %>
              </ul>
              <ul class="category-row <%= levelClass %>">
            <% } %>
            <%= SC.macros.navigationTabs(sub, false, level + 1) %>
          <% }) %>
        </ul>
			</div>
		</li>
	<% } else { %>
		<li class="dropdown-submenu <%= levelClass %>">
			<a <%= _.objectToAtrributes(item) %>>
				<%= item.text %>
			</a>
			<div class="dropdown-menu <%= levelClass %>">
        <ul class="category-row <%= levelClass %>">
          <% _.each(item.categories, function (sub, index) { %>
            <% if (index % MAX_CATEGORY_ROWS === 0 && index > 0) { %>
              </ul>
              <ul class="category-row <%= levelClass %>">
            <% } %>
            <%= SC.macros.navigationTabs(sub, false, level + 1) %>
          <% }) %>
        </ul>
			</div>
		</li>
	<% } %>
<% } else { %>
<li class="dropdown <%= levelClass %>">
	<a <%= _.objectToAtrributes(item) %> class="<%= levelClass %>">
		<%= item.text %>
	</a>
</li>
<% } %>
<% }) %>
