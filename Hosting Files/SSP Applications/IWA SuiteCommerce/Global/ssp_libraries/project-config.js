/*
 * Must also be valid JSON object after the ' = ', in order to work locally
 * Don't panic, new Object() is used instead of brackets for local parsing purposes
 * Therefore, DON'T USE BRACKETS BEFORE OR AFTER THE CONFIG ONES
 */
var SC = SC || new Object();
SC.projectConfig = {
    "local": {
        "host": "localhost/",
        "folder": "iwa/",
        "hosting_folder": "Hosting Files/"
    },
    "hosting_files_folder": "IWA Hosting Files",
    "urlroots": {
        "global": "global",
        "shopflow": "shopflow",
        "myaccount": "myaccount",
        "checkout": "checkout"
    },
    "site": {
        "categories": {
            "enable": true,
            "home_id": -200,
            "secure_enable": true,
            "secure_enable_subcategories": true
        },
        "content": {
            "enable": true,
            "secure_enable": false
        },
        "showOrdesAlsoFromSites": []
    },
    "precedences": {
        "global": [
            "IWA SuiteCommerce/Global/"
        ],
        "shopflow": [
            "IWA Reference/Reference ShopFlow 1.04.0/",
            "IWA SuiteCommerce/Global/",
            "IWA SuiteCommerce/ShopFlow/"
        ],
        "myaccount": [
            "IWA Reference/Reference My Account 1.03.0/",
            "IWA SuiteCommerce/Global/",
            "IWA SuiteCommerce/MyAccount/"
        ],
        "checkout": [
            "IWA Reference/Reference Checkout 2.02.0/",
            "IWA SuiteCommerce/Global/",
            "IWA SuiteCommerce/Checkout/"
        ]
    },
    "async_apps": ["shopflow"],
    "combiners": {
        "suitelet": {
            "script": "customscript_ns_sca_trigger_combiners",
            "deploy": "customdeploy_ns_sca_trigger_combiners"
        },
        "publisher": "IWA SuiteCommerce",
        "applications": {
            "shopflow" : {
                "folder": "ShopFlow",
                "combine": ["js", "js/libs", "skins/standard", "templates"]
            },
            "myaccount" : {
                "folder": "MyAccount",
                "combine": ["js", "skins/standard", "templates"]
            },
            "checkout" : {
                "folder": "Checkout",
                "combine": ["js", "skins/standard", "templates"]
            }
        },
        "password": {
            "required": false,
            "value": "CombineDaFil3s!"
        }
    }
};