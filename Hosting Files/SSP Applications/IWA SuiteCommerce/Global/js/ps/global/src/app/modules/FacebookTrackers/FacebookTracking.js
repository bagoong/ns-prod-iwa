define("FacebookTracking", function () {
    var currency = SC.ENVIRONMENT.currentCurrency.code || "USD";

    var FacebookTracking = {

        loadScript: function () {


            !function (f, b, e, v, n, t, s) {
                if (f.fbq)return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        },

        trackSearchAndCategory: function () {

            try {
                var keywords = _.parseUrlOptions(Backbone.history.fragment).keywords || "";


                var event = keywords ? 'Search' : 'ViewContent';

                fbq('track', event);

                console.warn("track " + event);

            } catch (e) {

            }


        },

        trackProduct: function (item) {

            var itemId = item.get("_id");
            var itemPrice = item.get("_price");
            try {

                fbq('track', 'ViewContent', {
                    content_ids: [itemId],
                    content_type: 'product',
                    value: itemPrice,
                    currency: 'USD'
                });

                console.warn("track product");
                console.warn(itemId);
                console.warn(itemPrice);

            } catch (e) {
                console.error(e);
            }
        },

        // this function is track the view cart page
        trackCart: function (lines) {

            try {
                var allProducts = [];
                var subTotal = 0;
                for (var i = 0; i < lines.length; i++) {
                    var item = lines[i].get("item");
                    var itemId = item.get("_id").toString();
                    var itemPrice = item.get("_price");
                    var itemQty = item.get("quantity");

                    subTotal += itemPrice * itemQty;
                    allProducts.push(itemId);
                }


                fbq('track', 'PageView');
                fbq('track', 'AddToCart', {
                    content_ids: allProducts,
                    content_type: 'product',
                    value: subTotal,
                    currency: 'USD'
                });


                console.warn("track cart");
                console.warn(allProducts);
                console.warn(subTotal);

            } catch (e) {
                console.error(e);
            }


        },
        // this function is track the add to cart event
        trackAddToCart: function (item) {
            try {
                var itemId = item.get("_id").toString();
                var itemPrice = item.get("_price");
                var itemQty = item.get("quantity");
                var totalPrice = itemPrice * itemQty;

                fbq('track', 'AddToCart', {
                    content_ids: [itemId],
                    content_type: 'product',
                    value: totalPrice,
                    currency: 'USD'
                });

                console.warn("track Add to cart");
                console.warn(itemId);
                console.warn(totalPrice);
            } catch (e) {
                console.error(e);
            }

        },

        trackTransaction: function (order) {



            try {
                var allProducts = [];

                var lines = order.get("lines");

                var total = 0;
                for (var i = 0; i < lines.length; i++) {
                    var item = lines.models[i].get("item");
                    allProducts.push(item.get("internalid"));
                    var price = (item.get("_price") && item.get("_price").toFixed(2)) || 0;
                    var quantity = item.get("quantity");
                    total += price * quantity;
                }

                fbq('track', 'Purchase', {
                    content_ids: allProducts,
                    content_type: 'product',
                    value: total,
                    currency: 'USD'
                });

                console.warn("track order");
                console.warn(allProducts);
                console.warn(total);


            } catch (e) {
                console.error(e)
            }


        },



        mountToApp: function (application) {

            if (SC.ENVIRONMENT.jsEnvironment === "browser") {

                var self = this,
                    layout = application.getLayout();

                application.trackers && application.trackers.push(FacebookTracking);

                application.trackEvent = _.wrap(application.trackEvent, function (fn) {
                    var options = _.toArray(arguments).slice(1),
                        category = options[0].category;

                    if (category == 'Add to Cart') {

                        var lineItemsOnCart = SC._applications.Shopping.getCart().get("lines").models;
                        var item = lineItemsOnCart[0].get("item");
                        self.trackAddToCart(item);
                    }
                    fn.apply(this, options);
                });

                layout.on("afterAppendView", function (view) {

                    console.warn("facebook module v5");

                    try {
                        self.loadScript();
                        fbq('init', '1128518953866515');
                        fbq('track', "PageView");

                } catch (e) {
                        console.error(e)
                    }

                    var tmpl = view.template;


                    switch (tmpl) {

                        case "product_details":
                            // add product pages tracker
                            self.trackProduct(view.model);
                            break;
                        case "shopping_cart":
                            //self.trackCart(view.model.get("lines").models);
                            break;
                        case "facet_browse": //aca va la search tmb
                            self.trackSearchAndCategory();
                            break;
                        default:
                            try{
                                fbq('track', 'ViewContent');

                            }catch(e){
                                console.error(e);
                            }

                            break;
                    }

                });
            }
        }
    };
    return FacebookTracking;
});