(function (SC) {

    'use strict';

    // application configuration
    // if needed, the second argument - omitted here - is the application name ('Shopping', 'MyAccount', 'Checkout')
    _.each(SC._applications, function(application) {

        application.on('beforeStartGlobal', function() {

            var configuration = application.Configuration,
                   $ = jQuery;

            /*
               Added itemImageFlatten function that is being
               used by the ItemsKeyMapping overwritten _thumbnail function
               */
            var itemImageFlatten = function (images) {
              if ('url' in images && 'altimagetext' in images) {
                return [images];
              }

              return _.flatten(_.map(images, function (item) {
                if (_.isArray(item)) {
                  return item;
                }

                return itemImageFlatten(item);
              }));
            }

            configuration.vimeoImageNotAvailable = '/Images/css-images/vimeo_loading.jpg';

            var vimeoThumbnail = configuration.vimeoImageNotAvailable;

            var getIframeSrc = function (iframe) {
              return iframe.match(/^.*src="(.*?)".*/)[1];
            }

            var isYouTubeVideo = function (iframe) {
              return /\/\/(?:www\.)?youtube\./.test(iframe);
            }

            var getYouTubeHash = function (iframe) {
              return getIframeSrc(iframe).match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/)[7];
            }

            var isVimeoVideo = function (iframe) {
              return /\/\/(?:www\.)?player\.vimeo\.com\/video\//.test(iframe);
            }

            var getVimeoHash = function (iframe) {
              return getIframeSrc(iframe).match(/\/\/(?:www\.)?player\.vimeo\.com\/video\/(\d*)/)[1];
            }

            var addVideoThumbnail = function (iframe, images) {
              var vimeoUrl, url, altText;

              if (isYouTubeVideo(iframe)) {
                url = '//img.youtube.com/vi/' + getYouTubeHash(iframe) + '/hqdefault.jpg';
                altText = 'YouTube Video';
              } else if (isVimeoVideo(iframe)) {
                url = vimeoThumbnail;
                vimeoUrl = '//vimeo.com/api/v2/video/' + getVimeoHash(iframe) + '.json';
                $.getJSON(vimeoUrl, function (data) {
                  url = data[0].thumbnail_small;
                  $('ul.bxslider').data('item-slider-loaded').done(function () {
                    $('.bx-pager-item').find('img[src^="' + vimeoThumbnail + '"]').attr('src', url);
                  });
                });
                altText = 'Vimeo Video';
              }

              images.push({
                url: url,
                altimagetext: altText,
                video: iframe
              });
            }

            /* add modules */
            application.addModule('Content.EnhancedViews.Extensions');
            application.addModule(['Categories',{ addToNavigationTabs:true, navigationAddMethod: 'prepend' }]);
            application.addModule('Facets.Model.SortFix');
            application.addModule('Facets.Translator.Categories');
            application.addModule('NavigationHelper.Extensions');
            application.addModule('SiteSearch.Extensions');
            application.addModule('Utils.Extension');
            application.addModule('LayoutGlobal.Extension');
            application.addModule('NewsletterSignup');
            application.addModule('FacebookTracking');
            application.addModule('GoogleTagManager');


            configuration.itemOptions = configuration.itemOptions || [];
            _.extend(configuration.itemOptions, [
                {
                    cartOptionId: 'custcol_is_gift_wrap',
                    itemOptionId: 'custitem_is_gift_wrap',
                    macros: {
                        selector: 'itemDetailsOptionHidden',
                        selected: 'shoppingCartOptionHidden'
                    }
                },
                {
                    cartOptionId: 'custcol_gift_wrap',
                    itemOptionId: 'custitem_gift_wrap',
                    macros: {
                        selected: 'shoppingCartOptionHidden',
                        selector: 'itemOptionGiftWrap'
                    }
                },
                {
                    cartOptionId: 'custcol_gift_wrap_package',
                    itemOptionId: 'custitem_gift_wrap_package',
                    macros: {
                        selector: 'itemDetailsOptionHidden',
                        selected: 'shoppingCartOptionHidden'
                    }
                }
            ]);

            _.extend(configuration, {

              logoUrl: '/Images/css-images/main-logo3.PNG',

              landingsMenu: SC.ENVIRONMENT.landingsMenu,

              navigationTabs: [],

              itemKeyMapping: _.extend(configuration.itemKeyMapping, {

                _storedescription: 'storedescription',

                _thumbnail: function (item) {
                  var item_images_detail = item.get('itemimages_detail') || {};

                  if (item_images_detail.thumbnail) {
                    if (item_images_detail.thumbnail.urls && item_images_detail.thumbnail.urls.length) {
                      return item_images_detail.thumbnail.urls[0];
                    }
                    return item_images_detail.thumbnail;
                  } else {
                    for (var cat in item_images_detail) {
                      if (cat.indexOf('main') != -1) {
                        if (item_images_detail[cat].urls && item_images_detail[cat].urls.length) {
                          return item_images_detail[cat].urls[0];
                        }
                        return item_images_detail[cat];
                      }
                    }

                    for (var cat in item_images_detail) {
                      var category = item_images_detail[cat];

                      for (var c in category) {
                        if (c.indexOf('main') != -1) {
                          if (category[c].urls && category[c].urls.length) {
                            return category[c].urls[0];
                          }
                          return category[c];
                        }
                      }
                    }
                  }

                  // otherwise it will try to use the storedisplaythumbnail
                  if (item.get('storedisplaythumbnail')) {
                    return {
                      url: item.get('storedisplaythumbnail'),
                      altimagetext: item.get('_name')
                    };
                  }
                  // No images huh? carry on

                  var parent_item = item.get('_matrixParent');
                  // If the item is a matrix child, it will return the thumbnail of the parent
                  if (parent_item && parent_item.get('internalid')) {
                    return parent_item.get('_thumbnail');
                  }

                  var images = itemImageFlatten(item_images_detail);

                  // If you using the advance images features it will grab the 1st one
                  if (images.length) {
                    return images[0];
                  }

                  // still nothing? image the not available
                  return {
                    url: configuration.imageNotAvailable,
                    altimagetext: item.get('_name')
                  };
                },

                _images: function (item) {
                  var result = [],
                      selected_options = item.itemOptions,
                      item_images_detail = item.get('itemimages_detail') || {},
                      swatch = selected_options && selected_options[application.getConfig('multiImageOption')] || null,
                      video = item.get('custitemiframe');

                  item_images_detail = item_images_detail.media || item_images_detail;

                  if (swatch && item_images_detail[swatch.label]) {
                    result = itemImageFlatten(item_images_detail[swatch.label]);
                  } else {
                    var aux = [];

                    _.each(item_images_detail, function(image, index) {
                      if (index.indexOf('main') == (index.length - 4)) {
                        aux.unshift(image);
                      } else {
                        if (image && image.length > 0) {
                          aux = _.union(aux, image);
                        } else {
                          aux.push(image);
                        }
                      }
                    });
                    result = itemImageFlatten(aux);
                  }

                  if (video) {
                    addVideoThumbnail(video, result);
                  }

                  return result.length ? result : [{
                    url: item.get('storedisplayimage') || configuration.imageNotAvailable,
                    altimagetext: item.get('_name')
                  }];
                },

                _breadcrumb: function (item) {
                  var breadcrumb = [];

                  if (item.get('defaultcategory_detail')) {
                    var category_path = '';

                    _.each(item.get('defaultcategory_detail'), function (cat) {
                      if (cat.url) {
                        category_path += '/'+cat.url;
                      }

                      breadcrumb.push({
                        href: category_path,
                        text: cat.label
                      });
                    });
                  } else {
                    breadcrumb.push({
                      href: '/',
                      text: _('Home').translate()
                    });
                  }

                  breadcrumb.push({
                    href: item.get('_url'),
                    text: item.get('_name')
                  });

                  return breadcrumb;
                },

                thumbnailName: function(item) {
                  var result = [],
                      selected_options = item.itemOptions,
                      item_images_detail = item.get('itemimages_detail') || {},
                      swatch = selected_options && selected_options[application.getConfig('multiImageOption')] || null;

                  item_images_detail = item_images_detail.media || item_images_detail;

                  if (item_images_detail.thumbnail) {
                    return 'main';
                  } else {
                    for (var cat in item_images_detail) {
                      if (cat.indexOf('main') != -1) {
                        return cat;
                      }
                    }

                    for (var cat in item_images_detail) {
                      var category = item_images_detail[cat];

                      for (var c in category) {
                        if (c.indexOf('main') != -1) {
                          return c;
                        }
                      }
                    }
                  }

                  return '';
                }
              })

            });

        });

    });

}(SC));
