define('Utils.Extension', function () {

  'use strict';

  var utils = {

    validateEmail: function (email) {
      var validEmailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (email.length === 0) {
        return _('Email address is required').translate();
      } else if (!validEmailRegex.test(email)) {
        return _('Valid email address is required').translate();
      } else {
        return null;
      }
    },

    validatePassword: function (password) {
      var MIN_CHARACTER_LONG = 8;

      if (password.length === 0) {
        return _('Password is required.').translate();
      } else if (password.length < MIN_CHARACTER_LONG) {
        return _('Password must be at least ' + MIN_CHARACTER_LONG + ' character long.').translate();
      } else if (!/[A-Za-z]/.test(password)) {
        return _('Password must contain at least one letter.').translate();
      } else if (!/[0-9!@#$%^&*.:;~'`*",_|= \<\>\/\\\+\?\-\(\)\[\]\{\}]/.test(password)) {
        return _('Password must contain at least one number or special characters').translate();
      } else if (!/^[A-Za-z0-9!@#$%^&*.:;~'`*",_|= \<\>\/\\\+\?\-\(\)\[\]\{\}]+$/.test(password)) {
        return _('Password may contain only letters, numbers and special characters.').translate();
      } else {
        return null;
      }
    },

    validateAddress: function (address) {
      if (address.length === 0) {
        return _('Address is required').translate();
      } else if (/^\d+$/.test(address)) {
        return _('Address can not be only numbers.').translate();
      } else {
        return null;
      }
    },

    validateFirstName: function (firstName) {
      if (firstName.length === 0) {
        return _('First Name is required.').translate();
      } else if (/[\"@&#\|~,\!\?\$\^\*\-\+\\=%;<>\/0-9]/.test(firstName)) {
        return _('First Name should not contain invalid characters').translate();
      } else {
        return null;
      }
    },

    validateLastName: function (lastName) {
      if (lastName.length === 0) {
        return _('Last Name is required.').translate();
      } else if (/[\"@&#\|~,\!\?\$\^\*\-\+\\=%;<>\/0-9]/.test(lastName)) {
        return _('Last Name should not contain invalid characters').translate();
      } else {
        return null;
      }
    },

    validateFullName: function (fullName) {
      if (fullName.length === 0) {
        return _('Full Name is required.').translate();
      } else if (/[\"@&#\|~,\!\?\$\^\*\-\+\\=%;<>\/0-9]/.test(fullName)) {
        return _('Full Name should not contain invalid characters').translate();
      } else {
        return null;
      }
    },

    isMobile: function () {
      if (!jQuery.browser) {
        (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
      }

      return jQuery.browser.mobile;
    },

    formatCurrency: function (value, symbol) {
      var sign = '',
          value_float = parseFloat(value);

      if (isNaN(value_float)) {
        return value;
      }

      if (value_float < 0) {
        sign = '-';
      }

      value_float = Math.abs(value_float);
      value_float = parseInt((value_float + 0.005) * 100, 10);
      value_float = value_float / 100;
      value_float = value_float.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
      var value_string = value_float.toString();

      // if the string doesn't contains a .
      if (!~value_string.indexOf('.')) {
        value_string += '.00';
      }
      // if it only contains one number after the .
      else if (value_string.indexOf('.') === (value_string.length - 2)) {
        value_string += '0';
      }

      symbol = symbol || SC.ENVIRONMENT.siteSettings.shopperCurrency.symbol || '$';

      return sign + symbol + value_string;
    },

    paymenthodIdCreditCart: function (cc_number) {
      // regex for credit card issuer validation
      var cards_reg_ex = {
        'Visa': /^4[0-9]{12}(?:[0-9]{3})?$/,
        'MasterCard': /^5[1-5][0-9]{14}$/,
        'American Express': /^3[47][0-9]{13}$/,
        'Discover': /^6(?:011|5[0-9]{2})[0-9]{12}$/
      },
      // get the credit card name 
      paymenthod_name;

      // validate that the number and issuer
      _.each(cards_reg_ex, function (reg_ex, name) {
        if (reg_ex.test(cc_number)) {
          paymenthod_name = name;
        }
      });

      var paymentmethod = paymenthod_name && _.findWhere(SC.ENVIRONMENT.siteSettings.paymentmethods, {
        name: paymenthod_name.toString()
      });

      return paymentmethod && paymentmethod.internalid;
    }

  };

  _.extend(SC.Utils, utils);
  _.mixin(utils);

});
