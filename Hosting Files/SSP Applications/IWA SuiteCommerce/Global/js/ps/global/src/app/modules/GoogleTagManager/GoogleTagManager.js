/**
 * Created by martin on 4/2/16.
 */
define("GoogleTagManager", function () {
    var tagManagerID,
        currency = SC.ENVIRONMENT.currentCurrency.code || "USD";

    function trackEvent(d) {

        if (!d.event) {
            d.event = "Other";
        }

        var data = _.extend(d,{
            pageTitle: document.title,
            pageView: Backbone.history.fragment
        });

        TTDataLayer.push(data);

    }

    var GoogleTagManager = {
        trackHome: function () {

            var data = {
                event: "Home",
                visitorType: "low-value"
            };

            trackEvent(data);
        },
        trackCart: function (lines) {
            var products = [];

            _.each(lines, function (line) {

                var item = line.get("item");

                products.push({
                    id: item.get("internalid"),
                    sku: item.get("_sku"),
                    price: (item.get("_price") && item.get("_price").toFixed(2)) || 0,
                    name: item.get("_name"),
                    quantity: line.get("quantity")
                });
            });

            var data = {
                event: "Cart",
                visitorType: "high-value",
                products: products
            };

            trackEvent(data);
        },
        trackProduct: function (item) {
            var data = {
                event: "productDetail",
                visitorType: "low-value",
                ecommerce: {
                    detail: {
                        products: [{
                            name: item.get("_name") || "",
                            id: item.get("internalid"),
                            sku: item.get("_sku"),
                            price: (item.get("_price") && item.get("_price").toFixed(2)) || 0,
                            quantity: item.get("quantity") || 1,
                            variant: ''
                        }]
                    }
                }
            };
            trackEvent(data);
        },
        trackProductList: function (collection) {

            var keywords = _.parseUrlOptions(Backbone.history.fragment).keywords || ""
                , items = collection.get("items")
                , list = keywords ? "Search Results" : "Category"
                , impressions = [];

            _.each(items.models, function (item, position) {
                impressions.push({
                    name: item.get("_name") || "",
                    id: item.get("internalid"),
                    sku: item.get("_sku"),
                    price: (item.get("_price") && item.get("_price").toFixed(2)) || 0,
                    quantity: item.get("quantity") || 1,
                    list: list,
                    position: position + 1
                });
            });

            var data = {
                event: "productList",
                visitorType: "low-value",
                ecommerce: {
                    currencyCode: currency,
                    impressions: impressions
                }
            };
            trackEvent(data);
        },
        trackCategory: function (view) {
            var data = {
                event: "Category",
                pageCategory: view.category.itemid,
                visitorType: "low-value"
            };
            trackEvent(data);
        },
        trackTransaction: function (order) {

            var products = [];

            order.get("lines").each(function (line) {
                var item = line.get("item");
                products.push({
                    id: item.get("internalid"),
                    sku: item.get("_sku"),
                    price: (item.get("_price") && item.get("_price").toFixed(2)) || 0,
                    name: item.get("_name"),
                    quantity: line.get("quantity")
                });
            });

            var data = {
                event: "orderConfirmation",
                visitorType: "high-value",
                transactionId: order.get("confirmation").internalid,
                transactionTotal: order.get("summary").total,
                transactionShipping: order.get("summary").shippingcost,
                transactionTax: order.get("summary").taxtotal,
                orderSubTotal: order.get("summary").subtotal,
                currency: currency,
                transactionProducts: products
            };
            trackEvent(data);
        },

        trackLoginRegister: function () {
            var data = {
                event: "Login/Register",
                pageCategory: "signup",
                visitorType: "high-value"
            };
            trackEvent(data);
        },
        mountToApp: function (application) {

            if (SC.ENVIRONMENT.jsEnvironment === "browser") {

                var config = application.getConfig("googleTagManager");

                    tagManagerID = "GTM-5BNJL4";

                    window.TTDataLayer = window.TTDataLayer || [];

                    jQuery.getScript("//www.googletagmanager.com/gtm.js?id=" + tagManagerID + "&l=TTDataLayer");

                    var self = this,
                        layout = application.getLayout();

                    application.trackers && application.trackers.push(GoogleTagManager);

                    //ADD CLICK EVENTS TO GTM
                    layout.once("afterAppendView", function (view) {
                        jQuery(document).on('click',function(e){
                            var data = {
                                event: "gtm.click",
                                'gtm.element': e.target,
                                'gtm.elementId': e.target.id,
                                'gtm.elementTarget': '',
                                'gtm.elementUrl':jQuery(e.target).attr('href') || '',
                                'gtm.elementClasses': e.target.className
                            };
                            trackEvent(data);
                        })
                    });

                    layout.on("afterAppendView", function (view) {

                        var tmpl = view.template;
                        switch (tmpl) {
                            case "home":
                                self.trackHome();
                                break;
                            case "product_details":
                                self.trackProduct(view.model);
                                break;
                            case "category_browse":
                                self.trackCategory(view);
                                break;
                            case "facet_browse":
                                self.trackProductList(view.model);
                                break;
                            case "shopping_cart":
                                self.trackCart(view.model.get("lines").models);
                                break;
                            case "login_register":
                                self.trackLoginRegister();
                                break;
                            default:
                                trackEvent({});
                                break;
                        }
                    });
            }
        }
    };
    return GoogleTagManager;
});