define('NewsletterSignup', function () {

  'use strict';

  var $ = jQuery;

  var NewsletterSignupModel = Backbone.Model.extend({
    url: _.getAbsoluteUrl('services/newsletterSignup.ss')
  });

  var NewsletterSignupView = Backbone.View.extend({

    template: 'newsletter_signup',

    events: {
      'submit #newsletter-signup-form': 'validateAndSubmitForm'
    },

    submitForm: function (email) {
      var $form = this.$('#newsletter-signup-form'),
          $signupMsg = this.$('#newsletter-msg'),
          model = new NewsletterSignupModel();

      model.save({
          email: email
        },{
        success: function (model, response) {
          $signupMsg.text(_('Thank you for subscribing to our email updates!').translate());
          $form.hide();
        },
        failure: function (model, response) {
          $signupMsg.text(_('Error when submitting the form.').translate());
        },
        complete: function () {
          $form.find('[type=email]').val('');
        }
      });
    },

    validateAndSubmitForm: function (event) {
      event.preventDefault();
      event.stopPropagation();

      var email = _.escape(this.$('[type=email]').val()),
          isEmailValid = _.validateEmail(email),
          $signupMsg = this.$('#newsletter-msg');

      if (_.isNull(isEmailValid)) {
        this.submitForm(email);
      } else {
        $signupMsg.text(isEmailValid);
      }

      $signupMsg.show();
    },

    render: function () {
      var $newsletterWrapper = this.options.application.getLayout().$('#newsletter-signup-footer-column');

      Backbone.View.prototype.render.apply(this, arguments);
      $newsletterWrapper.empty();
      $newsletterWrapper.html(this.$el);
    }

  });

  return {
    mountToApp: function (application, options) {
      application.getLayout().on('afterAppendView', function (view) {
        var view = new NewsletterSignupView({
          application: application
        });
        view.render();
      });
    }
  }

});