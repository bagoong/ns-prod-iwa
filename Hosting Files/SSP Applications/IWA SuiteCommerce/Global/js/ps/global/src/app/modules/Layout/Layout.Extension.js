define('LayoutGlobal.Extension', function () {

  'use strict';

  var $ = jQuery;

  $(window).on('message', function (event) {
    var originalEvent = event.originalEvent,
        iframeClass, iframeHeight;

    if (originalEvent.origin === 'https://forms.netsuite.com') {
      iframeClass = originalEvent.data.iframeClass;
      iframeHeight = parseInt(originalEvent.data.iframeHeight, 10);
      $(iframeClass).height(iframeHeight);
    }
  });

  return {
    mountToApp: function (application) {
      var Layout = application.getLayout();

      var screen_width = jQuery(window).width();
      if (screen_width < 768) {
        _.extend(Layout.key_elements, {search: '#mobile-search-container'});

        _.extend(Layout.events, {
            'submit #mobile-search-container form': 'searchEventHandler'
            ,	  'focus #mobile-search-container input': 'focusEventHandler'
            ,	  'seeAll #mobile-search-container input': 'seeAllEventHandler'
            ,	  'processed #mobile-search-container input': 'processAnchorTags'
        });
      }

      _.extend(Layout, {

        events: _.extend(Layout.events, {
          'mouseenter li.level-0': 'showCategoriesDropdown',
          'mouseleave li.level-0': 'hideCategoriesDropdown',
          'click li.level-0 a': 'hideCategoriesDropdown'
        }),

        showCategoriesDropdown: function (event) {
          if (!_.isMobile()) {
            var $dropdown = $(event.currentTarget).find('.dropdown-menu.level-0');
            $dropdown.show();
          }
        },

        hideCategoriesDropdown: function (event) {
          var $dropdown = $(event.currentTarget).closest('.dropdown.level-0').find('.dropdown-menu.level-0');
          $dropdown.hide();
        }

      });

      Layout.on('afterAppendView', function (view) {
        if (SC.ENVIRONMENT.jsEnvironment === 'server') {
          $('.hidden-desktop, .visible-tablet, .visible-phone').remove();
        }
      });

      Layout.on('afterRender', function () {
        if (SC.ENVIRONMENT.jsEnvironment === 'browser') {

          $.getScript('/External-Pages/live-person-script.js').done(function (content) {
            var script = '<script>' + content + '</script>';
            $('body').append(script);
          });

       }
      });
    }
  }

});
