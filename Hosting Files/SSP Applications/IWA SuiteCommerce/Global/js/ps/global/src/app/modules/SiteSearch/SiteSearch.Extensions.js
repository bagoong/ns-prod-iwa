define('SiteSearch.Extensions', ['SiteSearch'], function(Module) {

    'use strict';

    var SiteSearch = Module.SiteSearch;

    var currentSearchOptions = function ()
    {
        var newOptions = [],
            currentOptions = SC.Utils.parseUrlOptions(window.location.href);

        _.each(currentOptions, function (value, key)
        {
            var lowerCaseKey = key.toLowerCase();

            if (lowerCaseKey === 'order' || lowerCaseKey === 'show' ||  lowerCaseKey === 'display')
            {
                newOptions.push(lowerCaseKey + '=' + value);
            }
        });

        var newOptionsStr = newOptions.join('&');

        if (newOptionsStr.length > 0)
        {
            newOptionsStr = '&' + newOptionsStr;
        }

        return newOptionsStr;
    };

    _.extend(SiteSearch, {

        getShoppingTouchpointUrl: function(fragment) {
            var target_touchpoint_name = 'home',
                touchpoints = this.getApplication().getConfig('siteSettings.touchpoints'),
                target_touchpoint = (touchpoints ? touchpoints[target_touchpoint_name] : '') || '';
            return _.fixUrl(target_touchpoint + (fragment? (~target_touchpoint.indexOf('?') ? '&' : '?') + 'fragment=' + encodeURIComponent(fragment) : ''));
        },
        getKeywordSearchUrl: function(value) {
            return this.getShoppingTouchpointUrl(this.getKeywordSearchFragment(value, true));
        },
        getKeywordSearchFragment: function(value, not_encode) {
            var searchFragment = this.getApplication().getConfig('defaultSearchUrl');
            if(value !== '') {
                searchFragment += '?keywords='+ value + currentSearchOptions();
            }
            return not_encode? searchFragment : encodeURI(searchFragment);
        },

        // method call on submit of the Search form
        searchEventHandler: function (e)
        {
            e.preventDefault();

            this.search(jQuery(e.target).find('input').val());
        }

    });

});