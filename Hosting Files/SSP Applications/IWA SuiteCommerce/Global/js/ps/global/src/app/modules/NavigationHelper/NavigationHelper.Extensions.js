define('NavigationHelper.Extensions', ['NavigationHelper'], function() {

    'use strict';

    return {
        mountToApp: function(application) {

            var Layout = application.getLayout();

            // Touchpoints navigation
            _.extend(Layout, {

                hrefApplicationPrefixes: ['mailto', 'tel'],

                isLinkWithApplicationPrefix: function(href) {
                    return ~_.indexOf(this.hrefApplicationPrefixes, href.split(':')[0]);
                },
                isKeepHref: function($element) {
                    return $element.attr('data-keep-href') === 'true';
                },

                clickEventListener: _.wrap(Layout.clickEventListener, function(fn, e) {
                    var anchor = jQuery(e.target),
                        href = this.getUrl(anchor) || '#';

                    if(this.isKeepHref(anchor)) {
                        return;
                    }

                    if(this.isLinkWithApplicationPrefix(href)) {
                        e.preventDefault();
                        window.location.href = href;
                    } else {
                        fn.apply(this, Array.prototype.slice.call(arguments, 1));
                    }

                }),

                fixNoPushStateLink: _.wrap(Layout.fixNoPushStateLink, function(fn, e) {

                    var anchor = jQuery(e.target),
                        href = this.getUrl(anchor) || '#';

                    if(this.isLinkWithApplicationPrefix(href)) {
                        return;
                    }
                    if(this.isKeepHref(anchor)) {
                        return;
                    }

                    return fn.apply(this, Array.prototype.slice.call(arguments, 1));
                }),

                getTargetTouchpoint: _.wrap(Layout.getTargetTouchpoint, function (fn, $target) {

                    var application = this.application,
                        target_data = $target.data(),
                        hashtag = target_data.hashtag,
                        new_url = '',
                        clean_hashtag = hashtag && hashtag.replace('#', '');

                    // If we already are in the target touchpoint then we return the hashtag or the original href.
                    // We don't want to absolutize this url so we just return it.
                    if (target_data.touchpoint === application.getConfig('currentTouchpoint'))
                    {
                        new_url = clean_hashtag ? ('#' + clean_hashtag) : this.getUrl($target);
                        new_url = target_data.keepOptions ? this.getKeepOptionsUrl($target) : new_url;
                        return new_url;
                    }

                    return fn.apply(this, Array.prototype.slice.call(arguments, 1));
                })

            });

        }
    };

});