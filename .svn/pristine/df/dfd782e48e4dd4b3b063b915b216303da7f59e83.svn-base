// ItemDetails.Model.js
// --------------------
// Represents 1 single product of the web store

define('ItemDetails.Model.Ext', ['ItemDetails.Model', 'Facets.Translator'], function (Model, FacetsTranslator) {

  'use strict';

	var getOrig = Model.prototype.get;

	_.extend(Model.prototype, {

		getGiftWraps: function () {
			return this.giftWraps;
		},

		setGiftWrap: function (giftWrap) {
			this.giftWrap = giftWrap;
		},

		getGiftWrapMessage: function () {
			return this.giftWrapsMessage;
		},

		setGiftWrapMessage: function (giftWrapsMessage) {
			this.giftWrapsMessage = giftWrapsMessage;
		},

		getAvailableFinishes: function () {
			return this.availlableFinishes;
		},

		getQuantityPricing: function() {
			var onlinecustomerpricedetails = this.get('_priceDetails'),
          prices = [];

			_.each(onlinecustomerpricedetails.priceschedule, function (price) {
				var min = price.minimumquantity || 1,
            quantity = min;

				if (price.maximumquantity) {
					quantity = quantity + ' - ' + (price.maximumquantity-1);
				} else {
					quantity = quantity + ' +';
				}

				prices.push({
          quantity: quantity,
				  price: price.price_formatted
        });

			});

			return prices;
		},

		get: function (attr) {
			var value = getOrig.apply(this, arguments);

			if (attr === 'storedisplayimage') {
				if (value) {
					value = '/Images/'+value;	
				}				
			}
			if (attr === 'storedisplaythumbnail') {
				if (value) {
					value = '/Images/'+value;
				}
				
			}
			return value;
		},
		
		// itemOptionsHelper.parseQueryStringOptions
		// Given a url query string, it sets the options in the model
	 	parseQueryStringOptions: function (options) {
			var self = this;
			_.each(options, function (value, name) {
				if (name === 'quantity') {
					self.setOption('quantity', value);
				} else if (name === 'cartitemid') {
					self.cartItemId = value;
				} else if (value && name) {
					value = decodeURIComponent(value);
					var option = self.getPosibleOptionByUrl(name);

					if (option) {
						if (option.values) {
							value = _.where(option.values, {label: value})[0] || _.where(option.values, {internalid: value})[0] || _.where(option.values, {internalid: value})[0];				
							self.setOption(option.cartOptionId, value);
						} else {
							self.setOption(option.cartOptionId, value);
						}
					}
									
				}
			});
		},

		getItemOptionLabel: function (option, value) {
			var optionsDetails = this.get('_optionsDetails');
			var optionField = _.findWhere(optionsDetails.fields, {internalid: option});
			var optionValue = _.findWhere(optionField.values, {internalid: value});
			return optionValue.label;
		}
		
	});

  return Model;

});
